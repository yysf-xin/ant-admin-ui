/**
 * 在生产环境 代理是无法生效的，所以这里没有生产环境的配置
 * -------------------------------
 * The agent cannot take effect in the production environment
 * so there is no configuration of the production environment
 * For details, please see
 * https://pro.ant.design/docs/deploy
 */

const process = require('process');

if (!process._ex) {
  process._ex = true
  process.on('uncaughtException', function(err: any) {
    // Prevent ECONNRESET errors
    // handle the error safely
    console.log(err)
  });
}

const targetUrl = 'http://127.0.0.1:8080';

export default {
  devServer: {
    host: '0.0.0.0',
    port: 8000,
    https: false,
  },
  dev: {
    '/api/turn/': {
      target: targetUrl,
      ws: true,
      secure: false,
      changeOrigin: true,
    },
    '/api/slog/14/ws': {
      ws: true,
      secure: false,
      changeOrigin: true,
      target: 'ws://fx-shaofeng.inn.sf-express.com:1080/',
    },
    '/api/slog/14/': {
      ws: false,
      target: 'http://fx-shaofeng.inn.sf-express.com:1080/',
      changeOrigin: true
    },
    '/api/slog/': {
      ws: true,
      target: targetUrl,
      changeOrigin: true,
      pathRewrite: { '^/api/slog/': '/' },
    },
    '/api/': {
      target: targetUrl,
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
    '/ws/slog/test/': {
      target: targetUrl,
      ws: true,
      secure: false,
      changeOrigin: true,
      pathRewrite: { '^/ws/slog/test/': '/' },
    },
  },
  test: {
    '/api/': {
      target: targetUrl,
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
  },
  pre: {
    '/api/': {
      target: targetUrl,
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
  },
};
