﻿export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './user/Login',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/dashboard',
    name: '控制面板',
    icon: 'smile',
    component: './dproxy/Dashboard',
  },
  {
    path: '/ssh',
    icon: 'icon-server',
    name: 'SSH服务器',
    component: './dproxy/SshServer',
    routes: [
      {
        path: './',
        redirect: 'server',
      },
      {
        path: 'server',
        component: './dproxy/components/SshServerTable.tsx',
      },
      {
        path: 'tunnel',
        component: './dproxy/components/TunnelTable.tsx',
      },
    ],
  },
  {
    path: '/ssh',
    redirect: '/ssh/server',
  },
  {
    path: '/proxy',
    icon: 'icon-proxy',
    name: '多级代理配置',
    component: './dproxy/ProxyTable',
  },
  {
    path: '/local_server',
    icon: 'icon-server1',
    name: '本地服务管理',
    component: './dproxy/LocalServerTable',
  },
  {
    path: '/turn',
    icon: 'icon-server',
    name: '隧道穿透服务',
    component: './dproxy/Turn',
    routes: [
      {
        path: './',
        redirect: 'server',
      },
      {
        path: 'server',
        component: './dproxy/components/TurnGroupTable',
      },
      {
        path: 'clients',
        component: './dproxy/components/TurnClientTable',
      },
      {
        path: 'client',
        component: './dproxy/components/TurnClient',
      },
    ],
  },
  {
    path: '/route_rule',
    icon: 'icon-rule',
    name: '路由规则配置',
    component: './dproxy/RouteRule',
    routes: [
      {
        path: './',
        redirect: 'rule',
      },
      {
        path: 'rule',
        component: './dproxy/components/RouteRuleTable',
      },
      {
        path: 'tcp',
        component: './dproxy/components/TcpForwardTable',
      },
      {
        path: 'proxy',
        component: './dproxy/components/ProxyPassTable',
      },
    ],
  },
  {
    path: '/logger',
    icon: 'icon-server',
    name: '日志分析管理',
    // component: 'logger/layout2',
    routes: [
      {
        path: './',
        redirect: 'analyse',
      },
      {
        path: 'analyse',
        name: '实时日志流',
        component: './logger/components/LogList',
      },
      {
        path: 'config',
        name: '日志配置',
        component: './logger/components/LogServiceTable'
      }
    ],
  },
  {
    path: '/tools',
    icon: 'icon-server',
    name: '工具列表',
    routes: [
      {
        path: './',
        redirect: 'json',
      },
      {
        name: 'JSON工具',
        path: 'json',
        component: 'tools/json/JsonViewer',
      },
      {
        name: 'WebRTC',
        path: 'webrtc',
        component: 'tools/webrtc/layout',
        routes: [
          {
            path: '.',
            redirect: 'screen',
          },
          {
            path: 'screen',
            name: 'WebRTC示例',
            component: 'tools/webrtc/components/ScreenCapture',
          },
        ],
      },
    ],
  },
  {
    name: '服务管理',
    path: '/service',
    icon: 'icon-server',
    routes: [
      {
        path: '.',
        redirect: 'v2fly/task',
      },
      {
        name: '代理任务',
        path: 'v2fly',
        component: 'service/v2fly/',
        routes: [
          {
            path: '.',
            redirect: 'task',
          },
          {
            path: 'task',
            component: 'service/v2fly/components/task',
          },
          {
            path: 'canUse',
            component: 'service/v2fly/components/canUse',
          },
        ],
      },
      {
        name: '定时任务管理',
        path: 'corn',
        component: 'service/corn/'
      }
    ],
  },
  {
    name: '系统管理',
    path: '/system',
    icon: 'icon-server',
  },
  {
    path: '/',
    redirect: '/dashboard',
  },
  {
    component: './404',
  },
];
