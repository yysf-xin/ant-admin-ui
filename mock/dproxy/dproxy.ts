import mockjs from 'mockjs';

export default {
  '/api/ssh': mockjs.mock({
    'data|100': [
      {
        'Id|+1': 1,
        CreatedAt: '@datetime',
        UpdatedAt: '@datetime',
        'Remote|1-10': '',
        'IdRsa|1-10': '',
        Enabled: '@boolean',
      },
    ],
    success: true,
  }),
  'POST /api/ssh': {
    success: false,
    errorMessage: '保存失败',
  },
  'POST /api/ssh/*': {
    success: true,
    message: '保存成功',
  },
  'PUT /api/ssh': {
    success: true,
    errorMessage: '保存成功',
  },

  '/api/tcp_forward': mockjs.mock({
    'data|100': [
      {
        'Id|+1': 1,
        CreatedAt: '@datetime',
        UpdatedAt: '@datetime',
        Name: '@first',
        SrcIp: '@ip',
        TarIp: '@ip',
        Enabled: '@boolean',
      },
    ],
    success: true,
  }),
  'POST /api/tcp_forward': {
    success: false,
    errorMessage: '保存失败',
  },
  'PUT /api/tcp_forward': {
    success: true,
    errorMessage: '保存成功',
  },

  '/api/tunnel': mockjs.mock({
    'data|100': [
      {
        'Id|+1': 1,
        CreatedAt: '@datetime',
        UpdatedAt: '@datetime',
        Name: '@first',
        Local: '@ip',
        Target: '@ip',
        Enabled: '@boolean',
      },
    ],
    success: true,
  }),
  'POST /api/tunnel': {
    success: false,
    errorMessage: '保存失败',
  },
  'PUT /api/tunnel': {
    success: true,
    errorMessage: '保存成功',
  },

  '/api/proxy': mockjs.mock({
    'data|100': [
      {
        'Id|+1': 1,
        CreatedAt: '@datetime',
        UpdatedAt: '@datetime',
        URL: '@url',
        UserName: '@first',
        Password: '',
        Enabled: '@boolean',
      },
    ],
    success: true,
  }),
  'POST /api/proxy': {
    success: false,
    errorMessage: '保存失败',
  },
  'PUT /api/proxy': {
    success: true,
    errorMessage: '保存成功',
  },

  '/api/route_rule': mockjs.mock({
    'data|100': [
      {
        'Id|+1': 1,
        CreatedAt: '@datetime',
        UpdatedAt: '@datetime',
        Host: '@ip',
        Service: '',
        Enabled: '@boolean',
      },
    ],
    success: true,
  }),
  'POST /api/route_rule': {
    success: false,
    errorMessage: '保存失败',
  },
  'PUT /api/route_rule': {
    success: true,
    errorMessage: '保存成功',
  },
};
