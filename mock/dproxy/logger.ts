import mockjs from "mockjs";
import {Request, Response} from "express";

export default {
  '/api/logger/list': (req: Request, res: Response) => {
    return res.json(mockjs.mock({
      'data|50-200': [
        {
          'id|+1': 1,
          'level|1': ['debug','info','warn','error'],
          'name': 'com.feng1.test.test',
          'msg': '@cparagraph(1, 80)',
        },
      ],
      success: true,
    }))
  },
  '/api/model/slog_service': {
    success: true,
    data: [{
      id: 1,
      label: '测试test',
      name: 'test',
      path: '',
    },{
      id: 2,
      label: '测试shopapi',
      path: '14/',
      name: 'shopapi',
    },{
      id: 3,
      label: '模拟测试',
      name: 'test',
      path: '/',
    }]
  },
  '/api/logger/watch': {
    success: true,
    data: [
      {
        id: 1,
        pattern: 'AllLogger.*.log'
      },
      {
        id: 2,
        pattern: 'WarnLogger.*.log'
      },
      {
        id: 3,
        pattern: 'SqlLogger.*.log'
      }
    ]
  }
}
