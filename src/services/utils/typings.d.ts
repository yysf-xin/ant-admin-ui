import {ProCoreActionType, ProSchema, ProSchemaComponentTypes} from "@ant-design/pro-utils/lib/typing";
import React from "react";
import {ActionType} from "@ant-design/pro-table/lib/typing";

interface RestApi<R = false> {
  NewObj: (m: R | any) => Promise<any>;
  DelObj: (params: any) => Promise<any>;
  UpdateObj: (params: any, m: R | any) => any;
  ListObjs: (params: any) => any;
}

export interface ModalRef {
  editModal: (modal: any) => void;
  getTableAction: () => ActionType
}


type ColumnRender<Entity,ExtraProps = unknown,ValueType = 'text'> = (dom: React.ReactNode, entity: Entity, index: number, action: ProCoreActionType | undefined, schema: ProSchema<Entity, ExtraProps, ProSchemaComponentTypes, ValueType> & {
  isEditable?: boolean;
  type: ComponentsType;
}) => React.ReactNode;
