import { history } from '@@/core/history';
import { ErrorShowType, RequestConfig } from '@@/plugin-request/request';
import { extend } from 'umi-request';
import { message, notification } from 'antd';

const DEFAULT_ERROR_PAGE = '/exception';

export declare interface ErrorInfoStructure {
  success: boolean;
  data?: any;
  msg?: string;
  code?: string;
  showType?: ErrorShowType;
  traceId?: string;
  host?: string;
}

function showMessage(errorInfo: ErrorInfoStructure) {
  console.log(errorInfo);
  if (errorInfo.success) {
    message.success({
      content: errorInfo.msg,
      key: 'success',
    });
    return;
  }

  switch (errorInfo?.showType) {
    case ErrorShowType.SILENT:
      // do nothing
      break;
    case ErrorShowType.WARN_MESSAGE:
      message.warn({
        content: errorInfo.msg,
        key: 'warn',
      });
      break;
    case ErrorShowType.ERROR_MESSAGE:
      message.error({
        content: errorInfo.msg,
        key: 'error',
      });
      break;
    case ErrorShowType.NOTIFICATION:
      notification.error({
        description: errorInfo.msg,
        message: errorInfo.code,
        key: 'error',
      });
      break;
    case ErrorShowType.REDIRECT:
      // @ts-ignore
      history.push({
        pathname: DEFAULT_ERROR_PAGE,
        query: { errorCode: errorInfo.code, errorMessage: errorInfo.msg },
      });
      // redirect to error page
      break;
    default:
      message.error({
        content: errorInfo.msg,
        key: 'error',
      });
      break;
  }
}

const requestConfig: RequestConfig = {
  prefix: '/api',
  errorHandler(error) {
    const errorInfo = {
      success: false,
      showType: ErrorShowType.NOTIFICATION,
      code: error.name + ' ' + error.response?.status,
      message: error.message,
    };
    showMessage(errorInfo);
    error.data = errorInfo;
    return Promise.reject(error);
  },
};

const request = extend(requestConfig);

request.use(async (ctx, next) => {
  await next();
  const { res } = ctx;
  if (res.success !== undefined) {
    if (!res.success) {
      showMessage(res);
    }
  }
});

export default request;

export { requestConfig };
