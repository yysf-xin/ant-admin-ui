import { RestApi } from '@/services/utils/typings';
import request from './request';

export function createRestApi<R = false>(url: string): RestApi<R> {
  return {
    NewObj(data: R | any) {
      return request.post(url, {
        data,
      });
    },
    DelObj(id: any) {
      return request.delete(url + '/' + id);
    },
    UpdateObj(id: any, m: R | any) {
      console.log(m);
      return request.put(url + '/' + id, {
        data: m,
      });
    },
    ListObjs(params: any) {
      return request
        .get(url, {
          params,
        })
        .then((res) => {
          if (res.success) {
            return res;
          } else {
            return Promise.reject(res);
          }
        });
    },
  };
}

export function EmptyFn(prev: any, next: any): boolean {
  return prev != next;
}
