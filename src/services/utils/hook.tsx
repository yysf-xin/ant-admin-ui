import {ProColumns} from '@ant-design/pro-table';
import React, {MutableRefObject, useEffect, useRef} from 'react';
import {ModalRef, RestApi} from '@/services/utils/typings';
import SwitchColumn from '@/components/Column/SwitchColumn';
import { EmptyFn } from '@/services/utils/rest';
import OperateColumn, { getDataKey } from '@/components/Column/OperateColumn';

export declare class ColumnsType {
  column: ProColumns;
}

export declare class ColumnOperate {
  modalFormRef: React.MutableRefObject<any>;
}

export function withColumnsSwitch(
  request: (record: any, checked: boolean) => Promise<any>,
  dataIndex: string,
  checkedFn: (record: any) => boolean | undefined,
  hideInForm?: boolean,
): ColumnsType {
  const column: ProColumns = {
    title: '状态',
    dataIndex: dataIndex,
    width: 80,
    valueType: 'switch',
    hideInSearch: true,
    hideInForm: !!hideInForm,
    shouldCellUpdate: EmptyFn,
    render: (e, record) => {
      return <SwitchColumn request={request} record={record} checkedFn={checkedFn} />;
    },
  };
  return {
    column: column,
  };
}
export function useColumnOperates<T>(api: RestApi, cb: (record: T) => {
  name: string
  key: string
  custom?: JSX.Element
}[], onClick?: (key: string | undefined, record: T) => void | undefined): ColumnsType & ColumnOperate {
  const modalFormRef = useRef<ModalRef>();
  const column: ProColumns = {
    title: '操作',
    valueType: 'option',
    width: 140,
    shouldCellUpdate: EmptyFn,
    render: (_, record) => {
      return OperateColumn({
        items: cb(record),
        onClick: (e) => {
          if (!modalFormRef.current) {
            return;
          }
          const key = getDataKey(e);
          if (key === 'edit') {
            modalFormRef.current.editModal(record);
          } else if (key === 'delete') {
            api.DelObj(record.Id).then(() => {
              if (!modalFormRef.current) {
                return;
              }
              modalFormRef.current.getTableAction()?.reload();
            });
          } else {
            // eslint-disable-next-line
            onClick && onClick(key,record)
          }
        },
      });
    },
  };
  return {
    column,
    modalFormRef: modalFormRef,
  };
}

export function useColumnOperate(api: RestApi): ColumnsType & ColumnOperate {
  return useColumnOperates(api,()=> [
    {
      name: '编辑',
      key: 'edit',
    },
    {
      name: '删除',
      key: 'delete',
    },
  ])
}

export function useDestroyed(): MutableRefObject<boolean> {
  const destroyed = useRef<boolean>(false);
  useEffect(() => {
      destroyed.current = false
      return () => {
        destroyed.current = true;
      }
    },[]);
  return destroyed;
}
