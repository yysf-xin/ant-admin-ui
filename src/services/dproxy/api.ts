import { createRestApi } from '@/services/utils/rest';
import request from '../utils/request'
import {RestApi} from "@/services/utils/typings";

const SshApi: RestApi & {
  enable: (id: any,enable: boolean) => Promise<any>
} = {
  ...createRestApi('/model/ssh_server'),
  enable(id: any,enable: boolean) {
    return request.post(`/model/ssh_server/${id}/enable`, {
      data: {
        Status: enable? 1: 0
      }
    })
  }
};

const UserApi = {
  login(values: API.LoginParams) {
    return request.post('/login/account',{
      data: values
    })
  },
  currentUser() {
    return request.get('/user/info')
  }
}

export { SshApi ,UserApi };
