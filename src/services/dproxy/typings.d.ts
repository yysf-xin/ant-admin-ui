type BaseModel = {
  Id?: string
  id?: string;
  CreatedAt?: string;
  UpdatedAt?: string;
  Status?: number;
};

type SshServer = BaseModel & {
  Remote?: string;
  IdRsa?: string;
  Enabled?: boolean;
};

type TcpForward = BaseModel & {
  Name: string;
  SrcIp: string;
  TarIp: string;
  Enabled: boolean;
};

type Proxy = BaseModel & {
  URL?: string;
  UserName?: string;
  Password?: string;
  Enabled?: boolean;
};

type TurnGroup = BaseModel & {
  Name?: string;
  Enabled?: boolean;
};

type TurnClient = BaseModel & {
  Name?: string;
  Enabled?: boolean;
  Endpoint: string;
};

type LocalServer = BaseModel & {
  Host?: string;
  Path?: string;
  Target?: string;
  Enabled?: boolean;
};

type TaskInfo = {
  Checking?: number;
  Total?: number;
  Percent?: number;
  Error?: number;
  Curr?: number;
};

type ProxyInfo = BaseModel & {
  Server: string;
  Port: int;
  Country?: string;
  Type?: boolean;
};

type CornConfig = BaseModel & {
  corn?: string;
  Enabled?: boolean;
};

type DictItem = {
  name: string
  value: any
  children?: DictItem[]
}

