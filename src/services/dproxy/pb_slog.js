/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.msg = (function() {

    /**
     * Namespace msg.
     * @exports msg
     * @namespace
     */
    var msg = {};

    /**
     * MsgType enum.
     * @name msg.MsgType
     * @enum {number}
     * @property {number} NONE=0 NONE value
     * @property {number} WATCH=1 WATCH value
     * @property {number} LOG_MSG=2 LOG_MSG value
     */
    msg.MsgType = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "NONE"] = 0;
        values[valuesById[1] = "WATCH"] = 1;
        values[valuesById[2] = "LOG_MSG"] = 2;
        return values;
    })();

    msg.Request = (function() {

        /**
         * Properties of a Request.
         * @memberof msg
         * @interface IRequest
         * @property {number|null} [seq] Request seq
         * @property {msg.MsgType|null} [type] Request type
         * @property {Uint8Array|null} [data] Request data
         */

        /**
         * Constructs a new Request.
         * @memberof msg
         * @classdesc Represents a Request.
         * @implements IRequest
         * @constructor
         * @param {msg.IRequest=} [properties] Properties to set
         */
        function Request(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Request seq.
         * @member {number} seq
         * @memberof msg.Request
         * @instance
         */
        Request.prototype.seq = 0;

        /**
         * Request type.
         * @member {msg.MsgType} type
         * @memberof msg.Request
         * @instance
         */
        Request.prototype.type = 0;

        /**
         * Request data.
         * @member {Uint8Array} data
         * @memberof msg.Request
         * @instance
         */
        Request.prototype.data = $util.newBuffer([]);

        /**
         * Creates a new Request instance using the specified properties.
         * @function create
         * @memberof msg.Request
         * @static
         * @param {msg.IRequest=} [properties] Properties to set
         * @returns {msg.Request} Request instance
         */
        Request.create = function create(properties) {
            return new Request(properties);
        };

        /**
         * Encodes the specified Request message. Does not implicitly {@link msg.Request.verify|verify} messages.
         * @function encode
         * @memberof msg.Request
         * @static
         * @param {msg.IRequest} message Request message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Request.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.seq != null && Object.hasOwnProperty.call(message, "seq"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.seq);
            if (message.type != null && Object.hasOwnProperty.call(message, "type"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.type);
            if (message.data != null && Object.hasOwnProperty.call(message, "data"))
                writer.uint32(/* id 3, wireType 2 =*/26).bytes(message.data);
            return writer;
        };

        /**
         * Encodes the specified Request message, length delimited. Does not implicitly {@link msg.Request.verify|verify} messages.
         * @function encodeDelimited
         * @memberof msg.Request
         * @static
         * @param {msg.IRequest} message Request message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Request.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Request message from the specified reader or buffer.
         * @function decode
         * @memberof msg.Request
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {msg.Request} Request
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Request.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.msg.Request();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.seq = reader.uint32();
                        break;
                    }
                case 2: {
                        message.type = reader.int32();
                        break;
                    }
                case 3: {
                        message.data = reader.bytes();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Request message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof msg.Request
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {msg.Request} Request
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Request.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Request message.
         * @function verify
         * @memberof msg.Request
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Request.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.seq != null && message.hasOwnProperty("seq"))
                if (!$util.isInteger(message.seq))
                    return "seq: integer expected";
            if (message.type != null && message.hasOwnProperty("type"))
                switch (message.type) {
                default:
                    return "type: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            if (message.data != null && message.hasOwnProperty("data"))
                if (!(message.data && typeof message.data.length === "number" || $util.isString(message.data)))
                    return "data: buffer expected";
            return null;
        };

        /**
         * Creates a Request message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof msg.Request
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {msg.Request} Request
         */
        Request.fromObject = function fromObject(object) {
            if (object instanceof $root.msg.Request)
                return object;
            var message = new $root.msg.Request();
            if (object.seq != null)
                message.seq = object.seq >>> 0;
            switch (object.type) {
            case "NONE":
            case 0:
                message.type = 0;
                break;
            case "WATCH":
            case 1:
                message.type = 1;
                break;
            case "LOG_MSG":
            case 2:
                message.type = 2;
                break;
            }
            if (object.data != null)
                if (typeof object.data === "string")
                    $util.base64.decode(object.data, message.data = $util.newBuffer($util.base64.length(object.data)), 0);
                else if (object.data.length >= 0)
                    message.data = object.data;
            return message;
        };

        /**
         * Creates a plain object from a Request message. Also converts values to other types if specified.
         * @function toObject
         * @memberof msg.Request
         * @static
         * @param {msg.Request} message Request
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Request.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.seq = 0;
                object.type = options.enums === String ? "NONE" : 0;
                if (options.bytes === String)
                    object.data = "";
                else {
                    object.data = [];
                    if (options.bytes !== Array)
                        object.data = $util.newBuffer(object.data);
                }
            }
            if (message.seq != null && message.hasOwnProperty("seq"))
                object.seq = message.seq;
            if (message.type != null && message.hasOwnProperty("type"))
                object.type = options.enums === String ? $root.msg.MsgType[message.type] : message.type;
            if (message.data != null && message.hasOwnProperty("data"))
                object.data = options.bytes === String ? $util.base64.encode(message.data, 0, message.data.length) : options.bytes === Array ? Array.prototype.slice.call(message.data) : message.data;
            return object;
        };

        /**
         * Converts this Request to JSON.
         * @function toJSON
         * @memberof msg.Request
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Request.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for Request
         * @function getTypeUrl
         * @memberof msg.Request
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        Request.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/msg.Request";
        };

        return Request;
    })();

    msg.WatchReq = (function() {

        /**
         * Properties of a WatchReq.
         * @memberof msg
         * @interface IWatchReq
         * @property {string|null} [service] WatchReq service
         * @property {number|null} [id] WatchReq id
         * @property {boolean|null} [unWatch] WatchReq unWatch
         */

        /**
         * Constructs a new WatchReq.
         * @memberof msg
         * @classdesc Represents a WatchReq.
         * @implements IWatchReq
         * @constructor
         * @param {msg.IWatchReq=} [properties] Properties to set
         */
        function WatchReq(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * WatchReq service.
         * @member {string} service
         * @memberof msg.WatchReq
         * @instance
         */
        WatchReq.prototype.service = "";

        /**
         * WatchReq id.
         * @member {number} id
         * @memberof msg.WatchReq
         * @instance
         */
        WatchReq.prototype.id = 0;

        /**
         * WatchReq unWatch.
         * @member {boolean} unWatch
         * @memberof msg.WatchReq
         * @instance
         */
        WatchReq.prototype.unWatch = false;

        /**
         * Creates a new WatchReq instance using the specified properties.
         * @function create
         * @memberof msg.WatchReq
         * @static
         * @param {msg.IWatchReq=} [properties] Properties to set
         * @returns {msg.WatchReq} WatchReq instance
         */
        WatchReq.create = function create(properties) {
            return new WatchReq(properties);
        };

        /**
         * Encodes the specified WatchReq message. Does not implicitly {@link msg.WatchReq.verify|verify} messages.
         * @function encode
         * @memberof msg.WatchReq
         * @static
         * @param {msg.IWatchReq} message WatchReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        WatchReq.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.service != null && Object.hasOwnProperty.call(message, "service"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.service);
            if (message.id != null && Object.hasOwnProperty.call(message, "id"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.id);
            if (message.unWatch != null && Object.hasOwnProperty.call(message, "unWatch"))
                writer.uint32(/* id 3, wireType 0 =*/24).bool(message.unWatch);
            return writer;
        };

        /**
         * Encodes the specified WatchReq message, length delimited. Does not implicitly {@link msg.WatchReq.verify|verify} messages.
         * @function encodeDelimited
         * @memberof msg.WatchReq
         * @static
         * @param {msg.IWatchReq} message WatchReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        WatchReq.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a WatchReq message from the specified reader or buffer.
         * @function decode
         * @memberof msg.WatchReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {msg.WatchReq} WatchReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        WatchReq.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.msg.WatchReq();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.service = reader.string();
                        break;
                    }
                case 2: {
                        message.id = reader.int32();
                        break;
                    }
                case 3: {
                        message.unWatch = reader.bool();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a WatchReq message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof msg.WatchReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {msg.WatchReq} WatchReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        WatchReq.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a WatchReq message.
         * @function verify
         * @memberof msg.WatchReq
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        WatchReq.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.service != null && message.hasOwnProperty("service"))
                if (!$util.isString(message.service))
                    return "service: string expected";
            if (message.id != null && message.hasOwnProperty("id"))
                if (!$util.isInteger(message.id))
                    return "id: integer expected";
            if (message.unWatch != null && message.hasOwnProperty("unWatch"))
                if (typeof message.unWatch !== "boolean")
                    return "unWatch: boolean expected";
            return null;
        };

        /**
         * Creates a WatchReq message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof msg.WatchReq
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {msg.WatchReq} WatchReq
         */
        WatchReq.fromObject = function fromObject(object) {
            if (object instanceof $root.msg.WatchReq)
                return object;
            var message = new $root.msg.WatchReq();
            if (object.service != null)
                message.service = String(object.service);
            if (object.id != null)
                message.id = object.id | 0;
            if (object.unWatch != null)
                message.unWatch = Boolean(object.unWatch);
            return message;
        };

        /**
         * Creates a plain object from a WatchReq message. Also converts values to other types if specified.
         * @function toObject
         * @memberof msg.WatchReq
         * @static
         * @param {msg.WatchReq} message WatchReq
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        WatchReq.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.service = "";
                object.id = 0;
                object.unWatch = false;
            }
            if (message.service != null && message.hasOwnProperty("service"))
                object.service = message.service;
            if (message.id != null && message.hasOwnProperty("id"))
                object.id = message.id;
            if (message.unWatch != null && message.hasOwnProperty("unWatch"))
                object.unWatch = message.unWatch;
            return object;
        };

        /**
         * Converts this WatchReq to JSON.
         * @function toJSON
         * @memberof msg.WatchReq
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        WatchReq.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for WatchReq
         * @function getTypeUrl
         * @memberof msg.WatchReq
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        WatchReq.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/msg.WatchReq";
        };

        return WatchReq;
    })();

    msg.SearchReq = (function() {

        /**
         * Properties of a SearchReq.
         * @memberof msg
         * @interface ISearchReq
         * @property {Array.<msg.SearchReq.IField>|null} [fields] SearchReq fields
         */

        /**
         * Constructs a new SearchReq.
         * @memberof msg
         * @classdesc Represents a SearchReq.
         * @implements ISearchReq
         * @constructor
         * @param {msg.ISearchReq=} [properties] Properties to set
         */
        function SearchReq(properties) {
            this.fields = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * SearchReq fields.
         * @member {Array.<msg.SearchReq.IField>} fields
         * @memberof msg.SearchReq
         * @instance
         */
        SearchReq.prototype.fields = $util.emptyArray;

        /**
         * Creates a new SearchReq instance using the specified properties.
         * @function create
         * @memberof msg.SearchReq
         * @static
         * @param {msg.ISearchReq=} [properties] Properties to set
         * @returns {msg.SearchReq} SearchReq instance
         */
        SearchReq.create = function create(properties) {
            return new SearchReq(properties);
        };

        /**
         * Encodes the specified SearchReq message. Does not implicitly {@link msg.SearchReq.verify|verify} messages.
         * @function encode
         * @memberof msg.SearchReq
         * @static
         * @param {msg.ISearchReq} message SearchReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SearchReq.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.fields != null && message.fields.length)
                for (var i = 0; i < message.fields.length; ++i)
                    $root.msg.SearchReq.Field.encode(message.fields[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified SearchReq message, length delimited. Does not implicitly {@link msg.SearchReq.verify|verify} messages.
         * @function encodeDelimited
         * @memberof msg.SearchReq
         * @static
         * @param {msg.ISearchReq} message SearchReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SearchReq.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a SearchReq message from the specified reader or buffer.
         * @function decode
         * @memberof msg.SearchReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {msg.SearchReq} SearchReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SearchReq.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.msg.SearchReq();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        if (!(message.fields && message.fields.length))
                            message.fields = [];
                        message.fields.push($root.msg.SearchReq.Field.decode(reader, reader.uint32()));
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a SearchReq message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof msg.SearchReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {msg.SearchReq} SearchReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SearchReq.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a SearchReq message.
         * @function verify
         * @memberof msg.SearchReq
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        SearchReq.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.fields != null && message.hasOwnProperty("fields")) {
                if (!Array.isArray(message.fields))
                    return "fields: array expected";
                for (var i = 0; i < message.fields.length; ++i) {
                    var error = $root.msg.SearchReq.Field.verify(message.fields[i]);
                    if (error)
                        return "fields." + error;
                }
            }
            return null;
        };

        /**
         * Creates a SearchReq message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof msg.SearchReq
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {msg.SearchReq} SearchReq
         */
        SearchReq.fromObject = function fromObject(object) {
            if (object instanceof $root.msg.SearchReq)
                return object;
            var message = new $root.msg.SearchReq();
            if (object.fields) {
                if (!Array.isArray(object.fields))
                    throw TypeError(".msg.SearchReq.fields: array expected");
                message.fields = [];
                for (var i = 0; i < object.fields.length; ++i) {
                    if (typeof object.fields[i] !== "object")
                        throw TypeError(".msg.SearchReq.fields: object expected");
                    message.fields[i] = $root.msg.SearchReq.Field.fromObject(object.fields[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a SearchReq message. Also converts values to other types if specified.
         * @function toObject
         * @memberof msg.SearchReq
         * @static
         * @param {msg.SearchReq} message SearchReq
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        SearchReq.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.fields = [];
            if (message.fields && message.fields.length) {
                object.fields = [];
                for (var j = 0; j < message.fields.length; ++j)
                    object.fields[j] = $root.msg.SearchReq.Field.toObject(message.fields[j], options);
            }
            return object;
        };

        /**
         * Converts this SearchReq to JSON.
         * @function toJSON
         * @memberof msg.SearchReq
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        SearchReq.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for SearchReq
         * @function getTypeUrl
         * @memberof msg.SearchReq
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        SearchReq.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/msg.SearchReq";
        };

        SearchReq.Field = (function() {

            /**
             * Properties of a Field.
             * @memberof msg.SearchReq
             * @interface IField
             * @property {number|null} [flag] Field flag
             * @property {string|null} [field] Field field
             * @property {string|null} [keyword] Field keyword
             */

            /**
             * Constructs a new Field.
             * @memberof msg.SearchReq
             * @classdesc Represents a Field.
             * @implements IField
             * @constructor
             * @param {msg.SearchReq.IField=} [properties] Properties to set
             */
            function Field(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * Field flag.
             * @member {number} flag
             * @memberof msg.SearchReq.Field
             * @instance
             */
            Field.prototype.flag = 0;

            /**
             * Field field.
             * @member {string} field
             * @memberof msg.SearchReq.Field
             * @instance
             */
            Field.prototype.field = "";

            /**
             * Field keyword.
             * @member {string} keyword
             * @memberof msg.SearchReq.Field
             * @instance
             */
            Field.prototype.keyword = "";

            /**
             * Creates a new Field instance using the specified properties.
             * @function create
             * @memberof msg.SearchReq.Field
             * @static
             * @param {msg.SearchReq.IField=} [properties] Properties to set
             * @returns {msg.SearchReq.Field} Field instance
             */
            Field.create = function create(properties) {
                return new Field(properties);
            };

            /**
             * Encodes the specified Field message. Does not implicitly {@link msg.SearchReq.Field.verify|verify} messages.
             * @function encode
             * @memberof msg.SearchReq.Field
             * @static
             * @param {msg.SearchReq.IField} message Field message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Field.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.field != null && Object.hasOwnProperty.call(message, "field"))
                    writer.uint32(/* id 1, wireType 2 =*/10).string(message.field);
                if (message.keyword != null && Object.hasOwnProperty.call(message, "keyword"))
                    writer.uint32(/* id 2, wireType 2 =*/18).string(message.keyword);
                if (message.flag != null && Object.hasOwnProperty.call(message, "flag"))
                    writer.uint32(/* id 3, wireType 0 =*/24).uint32(message.flag);
                return writer;
            };

            /**
             * Encodes the specified Field message, length delimited. Does not implicitly {@link msg.SearchReq.Field.verify|verify} messages.
             * @function encodeDelimited
             * @memberof msg.SearchReq.Field
             * @static
             * @param {msg.SearchReq.IField} message Field message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Field.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a Field message from the specified reader or buffer.
             * @function decode
             * @memberof msg.SearchReq.Field
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {msg.SearchReq.Field} Field
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Field.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.msg.SearchReq.Field();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 3: {
                            message.flag = reader.uint32();
                            break;
                        }
                    case 1: {
                            message.field = reader.string();
                            break;
                        }
                    case 2: {
                            message.keyword = reader.string();
                            break;
                        }
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a Field message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof msg.SearchReq.Field
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {msg.SearchReq.Field} Field
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Field.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a Field message.
             * @function verify
             * @memberof msg.SearchReq.Field
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            Field.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.flag != null && message.hasOwnProperty("flag"))
                    if (!$util.isInteger(message.flag))
                        return "flag: integer expected";
                if (message.field != null && message.hasOwnProperty("field"))
                    if (!$util.isString(message.field))
                        return "field: string expected";
                if (message.keyword != null && message.hasOwnProperty("keyword"))
                    if (!$util.isString(message.keyword))
                        return "keyword: string expected";
                return null;
            };

            /**
             * Creates a Field message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof msg.SearchReq.Field
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {msg.SearchReq.Field} Field
             */
            Field.fromObject = function fromObject(object) {
                if (object instanceof $root.msg.SearchReq.Field)
                    return object;
                var message = new $root.msg.SearchReq.Field();
                if (object.flag != null)
                    message.flag = object.flag >>> 0;
                if (object.field != null)
                    message.field = String(object.field);
                if (object.keyword != null)
                    message.keyword = String(object.keyword);
                return message;
            };

            /**
             * Creates a plain object from a Field message. Also converts values to other types if specified.
             * @function toObject
             * @memberof msg.SearchReq.Field
             * @static
             * @param {msg.SearchReq.Field} message Field
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            Field.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults) {
                    object.field = "";
                    object.keyword = "";
                    object.flag = 0;
                }
                if (message.field != null && message.hasOwnProperty("field"))
                    object.field = message.field;
                if (message.keyword != null && message.hasOwnProperty("keyword"))
                    object.keyword = message.keyword;
                if (message.flag != null && message.hasOwnProperty("flag"))
                    object.flag = message.flag;
                return object;
            };

            /**
             * Converts this Field to JSON.
             * @function toJSON
             * @memberof msg.SearchReq.Field
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            Field.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            /**
             * Gets the default type url for Field
             * @function getTypeUrl
             * @memberof msg.SearchReq.Field
             * @static
             * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
             * @returns {string} The default type url
             */
            Field.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
                if (typeUrlPrefix === undefined) {
                    typeUrlPrefix = "type.googleapis.com";
                }
                return typeUrlPrefix + "/msg.SearchReq.Field";
            };

            return Field;
        })();

        return SearchReq;
    })();

    msg.Response = (function() {

        /**
         * Properties of a Response.
         * @memberof msg
         * @interface IResponse
         * @property {number|null} [seq] Response seq
         * @property {msg.MsgType|null} [type] Response type
         * @property {number|null} [code] Response code
         * @property {string|null} [msg] Response msg
         * @property {Uint8Array|null} [data] Response data
         */

        /**
         * Constructs a new Response.
         * @memberof msg
         * @classdesc Represents a Response.
         * @implements IResponse
         * @constructor
         * @param {msg.IResponse=} [properties] Properties to set
         */
        function Response(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Response seq.
         * @member {number} seq
         * @memberof msg.Response
         * @instance
         */
        Response.prototype.seq = 0;

        /**
         * Response type.
         * @member {msg.MsgType} type
         * @memberof msg.Response
         * @instance
         */
        Response.prototype.type = 0;

        /**
         * Response code.
         * @member {number} code
         * @memberof msg.Response
         * @instance
         */
        Response.prototype.code = 0;

        /**
         * Response msg.
         * @member {string} msg
         * @memberof msg.Response
         * @instance
         */
        Response.prototype.msg = "";

        /**
         * Response data.
         * @member {Uint8Array} data
         * @memberof msg.Response
         * @instance
         */
        Response.prototype.data = $util.newBuffer([]);

        /**
         * Creates a new Response instance using the specified properties.
         * @function create
         * @memberof msg.Response
         * @static
         * @param {msg.IResponse=} [properties] Properties to set
         * @returns {msg.Response} Response instance
         */
        Response.create = function create(properties) {
            return new Response(properties);
        };

        /**
         * Encodes the specified Response message. Does not implicitly {@link msg.Response.verify|verify} messages.
         * @function encode
         * @memberof msg.Response
         * @static
         * @param {msg.IResponse} message Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Response.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.seq != null && Object.hasOwnProperty.call(message, "seq"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.seq);
            if (message.type != null && Object.hasOwnProperty.call(message, "type"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.type);
            if (message.code != null && Object.hasOwnProperty.call(message, "code"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint32(message.code);
            if (message.msg != null && Object.hasOwnProperty.call(message, "msg"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.msg);
            if (message.data != null && Object.hasOwnProperty.call(message, "data"))
                writer.uint32(/* id 5, wireType 2 =*/42).bytes(message.data);
            return writer;
        };

        /**
         * Encodes the specified Response message, length delimited. Does not implicitly {@link msg.Response.verify|verify} messages.
         * @function encodeDelimited
         * @memberof msg.Response
         * @static
         * @param {msg.IResponse} message Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Response.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Response message from the specified reader or buffer.
         * @function decode
         * @memberof msg.Response
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {msg.Response} Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Response.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.msg.Response();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.seq = reader.uint32();
                        break;
                    }
                case 2: {
                        message.type = reader.int32();
                        break;
                    }
                case 3: {
                        message.code = reader.uint32();
                        break;
                    }
                case 4: {
                        message.msg = reader.string();
                        break;
                    }
                case 5: {
                        message.data = reader.bytes();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Response message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof msg.Response
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {msg.Response} Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Response.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Response message.
         * @function verify
         * @memberof msg.Response
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Response.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.seq != null && message.hasOwnProperty("seq"))
                if (!$util.isInteger(message.seq))
                    return "seq: integer expected";
            if (message.type != null && message.hasOwnProperty("type"))
                switch (message.type) {
                default:
                    return "type: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            if (message.code != null && message.hasOwnProperty("code"))
                if (!$util.isInteger(message.code))
                    return "code: integer expected";
            if (message.msg != null && message.hasOwnProperty("msg"))
                if (!$util.isString(message.msg))
                    return "msg: string expected";
            if (message.data != null && message.hasOwnProperty("data"))
                if (!(message.data && typeof message.data.length === "number" || $util.isString(message.data)))
                    return "data: buffer expected";
            return null;
        };

        /**
         * Creates a Response message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof msg.Response
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {msg.Response} Response
         */
        Response.fromObject = function fromObject(object) {
            if (object instanceof $root.msg.Response)
                return object;
            var message = new $root.msg.Response();
            if (object.seq != null)
                message.seq = object.seq >>> 0;
            switch (object.type) {
            case "NONE":
            case 0:
                message.type = 0;
                break;
            case "WATCH":
            case 1:
                message.type = 1;
                break;
            case "LOG_MSG":
            case 2:
                message.type = 2;
                break;
            }
            if (object.code != null)
                message.code = object.code >>> 0;
            if (object.msg != null)
                message.msg = String(object.msg);
            if (object.data != null)
                if (typeof object.data === "string")
                    $util.base64.decode(object.data, message.data = $util.newBuffer($util.base64.length(object.data)), 0);
                else if (object.data.length >= 0)
                    message.data = object.data;
            return message;
        };

        /**
         * Creates a plain object from a Response message. Also converts values to other types if specified.
         * @function toObject
         * @memberof msg.Response
         * @static
         * @param {msg.Response} message Response
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Response.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.seq = 0;
                object.type = options.enums === String ? "NONE" : 0;
                object.code = 0;
                object.msg = "";
                if (options.bytes === String)
                    object.data = "";
                else {
                    object.data = [];
                    if (options.bytes !== Array)
                        object.data = $util.newBuffer(object.data);
                }
            }
            if (message.seq != null && message.hasOwnProperty("seq"))
                object.seq = message.seq;
            if (message.type != null && message.hasOwnProperty("type"))
                object.type = options.enums === String ? $root.msg.MsgType[message.type] : message.type;
            if (message.code != null && message.hasOwnProperty("code"))
                object.code = message.code;
            if (message.msg != null && message.hasOwnProperty("msg"))
                object.msg = message.msg;
            if (message.data != null && message.hasOwnProperty("data"))
                object.data = options.bytes === String ? $util.base64.encode(message.data, 0, message.data.length) : options.bytes === Array ? Array.prototype.slice.call(message.data) : message.data;
            return object;
        };

        /**
         * Converts this Response to JSON.
         * @function toJSON
         * @memberof msg.Response
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Response.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for Response
         * @function getTypeUrl
         * @memberof msg.Response
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        Response.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/msg.Response";
        };

        return Response;
    })();

    msg.Message = (function() {

        /**
         * Properties of a Message.
         * @memberof msg
         * @interface IMessage
         * @property {number|null} [id] Message id
         * @property {number|Long|null} [time] Message time
         * @property {number|null} [level] Message level
         * @property {string|null} [appName] Message appName
         * @property {string|null} [trace] Message trace
         * @property {string|null} [logger] Message logger
         * @property {string|null} [method] Message method
         * @property {string|null} [message] Message message
         * @property {string|null} [subMessage] Message subMessage
         */

        /**
         * Constructs a new Message.
         * @memberof msg
         * @classdesc Represents a Message.
         * @implements IMessage
         * @constructor
         * @param {msg.IMessage=} [properties] Properties to set
         */
        function Message(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Message id.
         * @member {number} id
         * @memberof msg.Message
         * @instance
         */
        Message.prototype.id = 0;

        /**
         * Message time.
         * @member {number|Long} time
         * @memberof msg.Message
         * @instance
         */
        Message.prototype.time = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * Message level.
         * @member {number} level
         * @memberof msg.Message
         * @instance
         */
        Message.prototype.level = 0;

        /**
         * Message appName.
         * @member {string} appName
         * @memberof msg.Message
         * @instance
         */
        Message.prototype.appName = "";

        /**
         * Message trace.
         * @member {string} trace
         * @memberof msg.Message
         * @instance
         */
        Message.prototype.trace = "";

        /**
         * Message logger.
         * @member {string} logger
         * @memberof msg.Message
         * @instance
         */
        Message.prototype.logger = "";

        /**
         * Message method.
         * @member {string} method
         * @memberof msg.Message
         * @instance
         */
        Message.prototype.method = "";

        /**
         * Message message.
         * @member {string} message
         * @memberof msg.Message
         * @instance
         */
        Message.prototype.message = "";

        /**
         * Message subMessage.
         * @member {string} subMessage
         * @memberof msg.Message
         * @instance
         */
        Message.prototype.subMessage = "";

        /**
         * Creates a new Message instance using the specified properties.
         * @function create
         * @memberof msg.Message
         * @static
         * @param {msg.IMessage=} [properties] Properties to set
         * @returns {msg.Message} Message instance
         */
        Message.create = function create(properties) {
            return new Message(properties);
        };

        /**
         * Encodes the specified Message message. Does not implicitly {@link msg.Message.verify|verify} messages.
         * @function encode
         * @memberof msg.Message
         * @static
         * @param {msg.IMessage} message Message message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Message.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.id != null && Object.hasOwnProperty.call(message, "id"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.id);
            if (message.time != null && Object.hasOwnProperty.call(message, "time"))
                writer.uint32(/* id 2, wireType 1 =*/17).sfixed64(message.time);
            if (message.level != null && Object.hasOwnProperty.call(message, "level"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.level);
            if (message.appName != null && Object.hasOwnProperty.call(message, "appName"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.appName);
            if (message.trace != null && Object.hasOwnProperty.call(message, "trace"))
                writer.uint32(/* id 5, wireType 2 =*/42).string(message.trace);
            if (message.logger != null && Object.hasOwnProperty.call(message, "logger"))
                writer.uint32(/* id 6, wireType 2 =*/50).string(message.logger);
            if (message.method != null && Object.hasOwnProperty.call(message, "method"))
                writer.uint32(/* id 7, wireType 2 =*/58).string(message.method);
            if (message.message != null && Object.hasOwnProperty.call(message, "message"))
                writer.uint32(/* id 8, wireType 2 =*/66).string(message.message);
            if (message.subMessage != null && Object.hasOwnProperty.call(message, "subMessage"))
                writer.uint32(/* id 9, wireType 2 =*/74).string(message.subMessage);
            return writer;
        };

        /**
         * Encodes the specified Message message, length delimited. Does not implicitly {@link msg.Message.verify|verify} messages.
         * @function encodeDelimited
         * @memberof msg.Message
         * @static
         * @param {msg.IMessage} message Message message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Message.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Message message from the specified reader or buffer.
         * @function decode
         * @memberof msg.Message
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {msg.Message} Message
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Message.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.msg.Message();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.id = reader.int32();
                        break;
                    }
                case 2: {
                        message.time = reader.sfixed64();
                        break;
                    }
                case 3: {
                        message.level = reader.int32();
                        break;
                    }
                case 4: {
                        message.appName = reader.string();
                        break;
                    }
                case 5: {
                        message.trace = reader.string();
                        break;
                    }
                case 6: {
                        message.logger = reader.string();
                        break;
                    }
                case 7: {
                        message.method = reader.string();
                        break;
                    }
                case 8: {
                        message.message = reader.string();
                        break;
                    }
                case 9: {
                        message.subMessage = reader.string();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Message message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof msg.Message
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {msg.Message} Message
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Message.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Message message.
         * @function verify
         * @memberof msg.Message
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Message.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.id != null && message.hasOwnProperty("id"))
                if (!$util.isInteger(message.id))
                    return "id: integer expected";
            if (message.time != null && message.hasOwnProperty("time"))
                if (!$util.isInteger(message.time) && !(message.time && $util.isInteger(message.time.low) && $util.isInteger(message.time.high)))
                    return "time: integer|Long expected";
            if (message.level != null && message.hasOwnProperty("level"))
                if (!$util.isInteger(message.level))
                    return "level: integer expected";
            if (message.appName != null && message.hasOwnProperty("appName"))
                if (!$util.isString(message.appName))
                    return "appName: string expected";
            if (message.trace != null && message.hasOwnProperty("trace"))
                if (!$util.isString(message.trace))
                    return "trace: string expected";
            if (message.logger != null && message.hasOwnProperty("logger"))
                if (!$util.isString(message.logger))
                    return "logger: string expected";
            if (message.method != null && message.hasOwnProperty("method"))
                if (!$util.isString(message.method))
                    return "method: string expected";
            if (message.message != null && message.hasOwnProperty("message"))
                if (!$util.isString(message.message))
                    return "message: string expected";
            if (message.subMessage != null && message.hasOwnProperty("subMessage"))
                if (!$util.isString(message.subMessage))
                    return "subMessage: string expected";
            return null;
        };

        /**
         * Creates a Message message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof msg.Message
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {msg.Message} Message
         */
        Message.fromObject = function fromObject(object) {
            if (object instanceof $root.msg.Message)
                return object;
            var message = new $root.msg.Message();
            if (object.id != null)
                message.id = object.id | 0;
            if (object.time != null)
                if ($util.Long)
                    (message.time = $util.Long.fromValue(object.time)).unsigned = false;
                else if (typeof object.time === "string")
                    message.time = parseInt(object.time, 10);
                else if (typeof object.time === "number")
                    message.time = object.time;
                else if (typeof object.time === "object")
                    message.time = new $util.LongBits(object.time.low >>> 0, object.time.high >>> 0).toNumber();
            if (object.level != null)
                message.level = object.level | 0;
            if (object.appName != null)
                message.appName = String(object.appName);
            if (object.trace != null)
                message.trace = String(object.trace);
            if (object.logger != null)
                message.logger = String(object.logger);
            if (object.method != null)
                message.method = String(object.method);
            if (object.message != null)
                message.message = String(object.message);
            if (object.subMessage != null)
                message.subMessage = String(object.subMessage);
            return message;
        };

        /**
         * Creates a plain object from a Message message. Also converts values to other types if specified.
         * @function toObject
         * @memberof msg.Message
         * @static
         * @param {msg.Message} message Message
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Message.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.id = 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, false);
                    object.time = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.time = options.longs === String ? "0" : 0;
                object.level = 0;
                object.appName = "";
                object.trace = "";
                object.logger = "";
                object.method = "";
                object.message = "";
                object.subMessage = "";
            }
            if (message.id != null && message.hasOwnProperty("id"))
                object.id = message.id;
            if (message.time != null && message.hasOwnProperty("time"))
                if (typeof message.time === "number")
                    object.time = options.longs === String ? String(message.time) : message.time;
                else
                    object.time = options.longs === String ? $util.Long.prototype.toString.call(message.time) : options.longs === Number ? new $util.LongBits(message.time.low >>> 0, message.time.high >>> 0).toNumber() : message.time;
            if (message.level != null && message.hasOwnProperty("level"))
                object.level = message.level;
            if (message.appName != null && message.hasOwnProperty("appName"))
                object.appName = message.appName;
            if (message.trace != null && message.hasOwnProperty("trace"))
                object.trace = message.trace;
            if (message.logger != null && message.hasOwnProperty("logger"))
                object.logger = message.logger;
            if (message.method != null && message.hasOwnProperty("method"))
                object.method = message.method;
            if (message.message != null && message.hasOwnProperty("message"))
                object.message = message.message;
            if (message.subMessage != null && message.hasOwnProperty("subMessage"))
                object.subMessage = message.subMessage;
            return object;
        };

        /**
         * Converts this Message to JSON.
         * @function toJSON
         * @memberof msg.Message
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Message.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for Message
         * @function getTypeUrl
         * @memberof msg.Message
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        Message.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/msg.Message";
        };

        return Message;
    })();

    msg.Watcher = (function() {

        /**
         * Constructs a new Watcher service.
         * @memberof msg
         * @classdesc Represents a Watcher
         * @extends $protobuf.rpc.Service
         * @constructor
         * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
         * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
         * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
         */
        function Watcher(rpcImpl, requestDelimited, responseDelimited) {
            $protobuf.rpc.Service.call(this, rpcImpl, requestDelimited, responseDelimited);
        }

        (Watcher.prototype = Object.create($protobuf.rpc.Service.prototype)).constructor = Watcher;

        /**
         * Creates new Watcher service using the specified rpc implementation.
         * @function create
         * @memberof msg.Watcher
         * @static
         * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
         * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
         * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
         * @returns {Watcher} RPC service. Useful where requests and/or responses are streamed.
         */
        Watcher.create = function create(rpcImpl, requestDelimited, responseDelimited) {
            return new this(rpcImpl, requestDelimited, responseDelimited);
        };

        /**
         * Callback as used by {@link msg.Watcher#watchFile}.
         * @memberof msg.Watcher
         * @typedef WatchFileCallback
         * @type {function}
         * @param {Error|null} error Error, if any
         * @param {msg.Message} [response] Message
         */

        /**
         * Calls WatchFile.
         * @function watchFile
         * @memberof msg.Watcher
         * @instance
         * @param {msg.IWatchReq} request WatchReq message or plain object
         * @param {msg.Watcher.WatchFileCallback} callback Node-style callback called with the error, if any, and Message
         * @returns {undefined}
         * @variation 1
         */
        Object.defineProperty(Watcher.prototype.watchFile = function watchFile(request, callback) {
            return this.rpcCall(watchFile, $root.msg.WatchReq, $root.msg.Message, request, callback);
        }, "name", { value: "WatchFile" });

        /**
         * Calls WatchFile.
         * @function watchFile
         * @memberof msg.Watcher
         * @instance
         * @param {msg.IWatchReq} request WatchReq message or plain object
         * @returns {Promise<msg.Message>} Promise
         * @variation 2
         */

        return Watcher;
    })();

    return msg;
})();

module.exports = $root;
