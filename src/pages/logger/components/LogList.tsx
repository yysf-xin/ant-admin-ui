import {Drawer, Typography,Empty,Alert } from 'antd';
// import moment, {DurationInputArg1, DurationInputArg2} from 'moment';
import {useRef} from "react";
import MessageView from './Logger'

import {FixedSizeList as VList} from 'react-window';
// @ts-ignore
import AutoResize from 'react-virtualized-auto-sizer';
import ServiceSelect from './ServiceSelect'
import {useSlogClient} from "../proto/client";

const { Paragraph } = Typography;

// const regionFn = (num: DurationInputArg1,unit?: DurationInputArg2) => {
//   return () => {
//     return [moment().subtract(num,unit),moment()]
//   }
// }


const LogList = () => {

  const filterRef = useRef(null);

  const {
    message,
    showMessage,
    itemData,
    setServiceVal,
    setWatchFile,
    service,
    watchFile,
    realTime,
    setRealTime,
    logData,
    doSearch,
    readyState
  } = useSlogClient(filterRef)

  const ref = useRef(null);


  // const [autoScroll,setAutoScroll] = useState<boolean>(false)
  // useEffect(()=>{
  //   if (autoScroll) {
  //   }
  // },[autoScroll])

  // const [,setWatch] = useState<WatchStatus|null>()
  // useEffect(()=>{
  //   if (!watch) {
  //     return
  //   }
  //   console.log('start watch',watch)
  //   request.get('/logger/list').then(res => {
  //     if (res.success && ref.current) {
  //       setItemData({
  //         items: res.data,
  //         show: showMessage
  //       })
  //     }
  //     // if (ref.current) {
  //     //   console.log(ref.current)
  //     // }
  //   })
  //   return () => {
  //
  //   }
  // },[watch])


  return <div style={{margin: -24,padding: 12,height: '100%'}}>
    <ServiceSelect
      setWatchFile={setWatchFile}
      setServiceVal={setServiceVal}
      watchValue={watchFile}
      serviceVal={service}
      realTime={realTime}
      setRealTime={setRealTime}
      logData={logData}
      doSearch={doSearch}
      readyState={readyState}
      filterRef={filterRef}
    />
    {itemData.buf.count == 0 && <><Empty description={<Alert message={"暂无数据"}/>} /></>}
    {itemData.buf.count != 0 &&
      <AutoResize>
        {({height, width}: { height: number, width: number }) => (
          <VList
            ref={ref}
            itemSize={80}
            height={height}
            itemKey={(index,data)=> {
              return data.buf.getByIndex(index).id
            }}
            itemCount={itemData.buf.count}
            width={width}
            itemData={itemData}>
            {MessageView}
          </VList>
        )}
      </AutoResize>
    }
    <Drawer className={"log-drawer"} title={"消息详情"} contentWrapperStyle={{
      maxWidth: '100%'
    }} width={800} visible={message != null} onClose={()=> showMessage(null)}>
      {
        message && <Paragraph className={'logger-msg2'}>
          <pre>{message.message}</pre>
          {message.subMessage && <pre>{message.subMessage}</pre>}
        </Paragraph>
      }
    </Drawer>
  </div>
}

export default LogList
