import {Badge, Button, Checkbox, Input, Select, Space} from "antd";
import {MutableRefObject, useCallback, useEffect, useRef, useState} from "react";
import {useDestroyed} from "@/services/utils/hook";
import request from "@/services/utils/request";
import {history} from 'umi';
import {LogData} from "@/pages/logger/proto/client";

const Levels = [{
  label: '调试',
  value: 0
},{
  label: '信息',
  value: 1
},{
  label: '警告',
  value: 2
},{
  label: '错误',
  value: 3
}]


const ServiceSelect = ({setWatchFile,setServiceVal,serviceVal,watchValue,realTime,setRealTime,doSearch,readyState,filterRef,logData}: {
  setWatchFile: (f: WatchFile|null) => void
  setServiceVal: (s: Service) => void
  serviceVal: Service|undefined
  watchValue: WatchFile|null
  realTime: boolean
  setRealTime: (s: boolean) => void
  logData: LogData
  doSearch: (s: SearchOpt) => void
  readyState: number
  filterRef: MutableRefObject<any>
}) => {

  const [services,setServices] = useState<Service[]>()
  const [watchFiles,setWatchFiles] = useState<WatchFile[]>()
  const searchInput = useRef<any>()
  const traceInput = useRef<any>()

  const [searchOpt,setSearchOpt] = useState<SearchOpt>({})
  const [traceVal,setTraceVal] = useState<string>()

  const destroyed = useDestroyed()

  useEffect(()=>{
    request.get('/model/slog_service').then(res => {
      if (destroyed.current) {
        return
      }
      res.data = res.data.map((ss: any) => {
        if (ss.Id) {
          ss.id = ss.Id
        }
        delete ss.Id
        return ss
      })
      setServices(res.data)
    })
  },[destroyed])


  const onWatchSelect = useCallback((id: number)=>{
    const query: {
      service?: string
      watch?: string
    } = history.location.query || {}

    if (query.watch && Number(query.watch) == id) {
      return
    }

    const wR = watchFiles?.find(w => w.id === id)
    if (wR) {
      query.watch = String(id)
      setWatchFile(wR)
    } else {
      delete query.watch
    }
    history.replace({
      query: query
    })

  },[setWatchFile, watchFiles])

  const onClear = () => {
    logData.all.clear()
    for (let i = 0; i < logData.levels.length; i++) {
      logData.levels[i].clear()
    }
    logData.searchResult.clear()
    if (logData.refreshUI) {
      logData.refreshUI(logData.all.count)
    }
  }

  const setServiceFn = useCallback((id: number) => {
    if (!services) {
      return
    }
    const r = services.find(s => s.id === id)
    if (r) {
      onClear()
      setServiceVal(r)

      setWatchFiles([])

      const query: {
        service?: string
        watch?: string
      } = history.location.query || {}

      if (query.service) {
        if (id !== Number(query.service)) {
          delete query.watch
          setWatchFile(null)
        }
      }

      query.service = String(id)

      setWatchFiles([])

      console.log(r.name)
      let url
      if (r.path.startsWith("//")) {
        url = r.path + r.name
      } else {
        url = '/slog/' + r.path + 'api/watch/' + r.name
      }
      console.log(url)
      // 'http://10.227.100.14:8080/api/watch/shopapi'
      // 'http://192.168.123.116:8080/api/watch/test'
      // http://127.0.0.1:8080/api/watch/test
      request.get(url).then(res => {
        if (destroyed.current) {
          return
        }
        const w = res.data as WatchFile[]
        setWatchFiles(w)

        if (query.watch) {
          const wId = Number(query.watch)
          const wR = w.find(s=> s.id === wId)
          if (wR) {
            setWatchFile(wR)
          }
        }
        history.replace({
          query: query
        })
      }).catch(console.error)
    }
  },[services, setServiceVal, setWatchFile, destroyed,logData])

  useEffect(() => {
    if (!services) {
      return
    }

    const query: {
      service?: string
      watch?: string
    } = history.location.query || {}

    if (query.service) {
      const sId = Number(query.service)
      setServiceFn(sId)
    }
  },[services,setServiceFn])



  const onLevelSelect = (e: number) => {
    searchOpt.level = e
    doSearch(searchOpt)
    setSearchOpt(searchOpt)
  }

  const onSearch = () => {
    if (!searchInput.current || !traceInput.current) {
      return
    }
    const val = searchInput.current.input.value.trim()
    const trace = traceInput.current.input.value.trim()

    if (searchOpt.keyword == val && searchOpt.trace == trace) {
      return;
    }
    searchOpt.keyword = val
    searchOpt.trace = trace
    doSearch(searchOpt)
    setSearchOpt(searchOpt)
  }

  filterRef.current = ({trace}: { trace: string }) => {
    if (!traceInput.current) {
      return
    }
    if (trace) {
      const idx = trace.indexOf(',')
      if (idx != -1) {
        trace = trace.substring(0, idx)
      }
      if (trace) {
        traceInput.current.input.value = trace
        setTraceVal(trace)
        onSearch()
      }
    }
  }

  return <Space align={"baseline"}>
    <Badge status={readyState == 1 ? 'success': 'error'} />
    <Select
      placeholder={'选择监测服务'}
      style={{width: 160}}
      value={serviceVal && serviceVal.id}
      showSearch={true}
      fieldNames={{
        label: 'label',
        value: 'id'
      }}
      onSelect={setServiceFn}
      optionFilterProp={'label'}
      filterOption={true}
      options={services}
    />
    <Select
      placeholder={'选择监测文件'}
      value={watchValue && watchValue.id}
      style={{width: 160}}
      showSearch={true}
      optionFilterProp={'pattern'}
      filterOption={true}
      options={watchFiles}
      fieldNames={{
        label: 'pattern',
        value: 'id'
      }}
      onSelect={onWatchSelect}
    />
    <Checkbox checked={realTime} onChange={(e) => {
      setRealTime(e.target.checked)
    }}>实时日志</Checkbox>
    <Select placeholder={"日志级别"} options={Levels} style={{width: 100}} allowClear={true} value={searchOpt?.level} onChange={onLevelSelect} />

    <Input ref={traceInput} value={traceVal} placeholder={'追踪码'} style={{width:120}} allowClear={true} onChange={
      (e) => setTraceVal(e.target.value)
    }/>
    <Input ref={searchInput} placeholder={'关键词'} style={{width:160}} allowClear={true} />

    <Button type={"primary"} onClick={onSearch}>搜索</Button>
    <Button onClick={onClear}>清空</Button>
  </Space>
}

export default ServiceSelect
