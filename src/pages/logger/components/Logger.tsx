import './logger.less'
import moment from "moment";
import {Button, Space, Tag } from 'antd';
import type {PresetColorType} from "antd/es/_util/colors";
import {memo} from "react";
import {areEqual, ListChildComponentProps} from "react-window";
import {RingLogBuff} from "@/pages/logger/proto/client";

const LevelStyle: {
  cls: string,
  color: PresetColorType
}[] = [{
  cls: 'ant-alert-info logger',
  color: 'blue'
}, {
  cls: 'ant-alert-success logger',
  color: "green"
}, {
  cls: 'ant-alert-warning logger',
  color: 'yellow'
}, {
  cls: 'ant-alert-error logger',
  color: "red"
}]
window.moment = moment
const MessageView = memo(({data,index,style}: ListChildComponentProps) => {
  const item = (data.buf as RingLogBuff).getByIndex(index)
  if (!item.time) {
    item.time = Date.now()
  }
  const cls = LevelStyle[item.level] || LevelStyle[0]
  return <div style={style}>
    <div className={cls.cls}>
      <Space>
        {moment(item.time).format('yyyy-MM-DD HH:mm:ss.SSS')}
        <Tag color={cls.color}>{item.id}</Tag>
        {item.appName}
        <Tag.CheckableTag checked={true} onClick={() => {
          data.filter.current({
            trace: item.trace
          })
        }}>{item.trace}</Tag.CheckableTag>
        {item.logger}
        {item.method}
        <Button type="link" onClick={() => data.show(item)}>更多</Button>
      </Space>
      <p className={"logger-msg"}>
        {item.message}
        {item.subMessage}
      </p>
    </div>
  </div>
},areEqual)


export default MessageView
