import CurdTable from '@/components/Rest/CurdTable';
import type { ProColumns } from '@ant-design/pro-table';
import React from 'react';
import { createRestApi, EmptyFn } from '@/services/utils/rest';
import { useColumnOperate } from '@/services/utils/hook';
import request from '@/services/utils/request';

const ProxyPassApi = {
  ...createRestApi('/model/slog_service'),
  enable(id: any, Enabled: boolean) {
    return request.post(`/model/slog_service/${id}/enable`, {
      data: {
        Enabled,
      },
    });
  },
};

const LogServiceTable: React.FC = () => {
  const operateCol = useColumnOperate(ProxyPassApi);
  const columns: ProColumns<Proxy>[] = [
    {
      dataIndex: 'Id',
      key: 'Id',
      title: 'Id',
      width: 80,
      formItemProps: {
        hidden: true,
      },
    },
    {
      title: '标签',
      dataIndex: 'label',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
        shouldUpdate: false,
      },
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '名称',
      dataIndex: 'name',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
        shouldUpdate: false,
      },
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '路径',
      dataIndex: 'path',
      copyable: true,
      width: 300,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
      },
      hideInSearch: true,
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '修改时间',
      width: 180,
      dataIndex: 'UpdatedAt',
      valueType: 'dateTime',
      hideInForm: true,
      shouldCellUpdate: EmptyFn,
    },
    operateCol.column,
  ];

  return (
    <CurdTable
      title={'日志监测配置'}
      columns={columns}
      api={ProxyPassApi}
      rowKey={'Id'}
      search={false}
      modalFormRef={operateCol.modalFormRef}
    />
  );
};
export default LogServiceTable;
