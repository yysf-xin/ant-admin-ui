declare type Message = {
  id: number,
  time: number
  appName: string
  level: number
  logger?: string
  method?: string
  trace?: string
  message: string
  subMessage?: string
}

declare type Service = {
  id: number,
  label: string
  name: string
  path: string
}

declare type WatchFile = {
  id: number
  pattern: string
}

declare type WatchStatus = {
  service: Service,
  watch: WatchFile
}

declare type SlogResponse = {
  type: number
  data?: Uint8Array
  seq: number
  code: number
  msg?: string
}

interface SearchOpt {
  keyword?: string
  level?: number
  trace?: string
}
