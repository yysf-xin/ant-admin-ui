// import { load } from "protobufjs";
import {useEffect, useRef, useState} from "react";
const $protobuf = require("protobufjs/minimal");
import {msg as pb} from '@/services/dproxy/pb_slog'
import {debounce} from "lodash";


type WsStatus = {
  open: boolean
}

type Options = {
  setReadyState: (n: number) => void
  logData: LogData
  refreshUI: (d: number) => void
  service: Service
}

class ConnectInfo {
  public conn: WebSocket
  public status: WsStatus
  private opt: Options | null;
  private f: WatchFile | undefined;
  // private seq: number
  private readonly map: Map<number, any>
  private watch: any;
  constructor(conn: WebSocket,opt: Options) {
    conn.binaryType = 'arraybuffer'
    this.conn = conn
    conn.onopen = this.onopen.bind(this)
    conn.onclose = this.onclose.bind(this)
    conn.onerror = this.onerror.bind(this)
    conn.onmessage = this.onmessage.bind(this)
    this.status = {
      open: false
    }
    this.opt = opt
    // this.seq = 0
    this.map = new Map<number, any>()

  }
  close(){
    this.opt = null
    this.conn.close()
  }

  private onopen(e: Event) {
    console.log('conn open',e)
    this.status.open = true

    const opt = this.opt
    if (opt) {
      opt.setReadyState(this.conn.readyState)
    }
  }

  private onclose(ev: CloseEvent) {
    console.log('conn closed',ev.code,ev.reason)
    this.status.open = false
    const opt = this.opt
    if (opt) {
      opt.setReadyState(this.conn.readyState)
    }
  }

  private onerror(ev: Event) {
    console.error('conn error',ev)
    this.status.open = false
    const opt = this.opt
    if (opt) {
      opt.setReadyState(this.conn.readyState)
    }
  }

  public attachFile(f: WatchFile|null) {
    if (!this.opt) {
      return
    }
    if (f) {
      this.watch = pb.Watcher.create((method, requestData, callback) => {
        this.map[0] = callback
        if (this.conn.readyState === 1) {
          const req = new pb.Request()
          req.seq = 0
          req.type = pb.MsgType.WATCH
          req.data = requestData
          const data = pb.Request.encode(req).finish()
          this.conn.send(data)
        }
      }, false, false)
      this.f = f
      console.log('attachFile',f)
    } else if (this.watch){
      console.log('detachFile',this.f)
    }

    if (this.f) {
      const watchReq = new pb.WatchReq()
      watchReq.id = this.f.id
      watchReq.service = this.opt.service.name
      watchReq.unWatch = !f
      this.watch.watchFile(watchReq,this.onFileNotify.bind(this))
    }
  }
  private onFileNotify(err: any, msg: Message) {
    // console.log('watchFile result', msg, err)
    if (this.opt && msg && msg.id) {
      const data = this.opt.logData
      data.all.push(msg)

      for(let i = 0; i <= msg.level; i++) {
        data.levels[i].push(msg)
      }

      if (data.searchOpt.trace) {
        if (!msg.trace) {
          return;
        }
        if (!msg.trace.includes(data.searchOpt.trace)) {
          return
        }
      }
      if (data.searchOpt.keyword) {
        if (msg.message.includes(data.searchOpt.keyword)) {
          data.searchResult.push(msg)
        }
      } else {
        data.searchResult.push(msg)
      }
      // console.log('刷新ui',msg)
      this.opt.refreshUI(msg.time)
    }
  }
  private onmessage(e: MessageEvent) {
    if (e.data.byteLength == 0) {
      return
    }
    try {
      const reader = new $protobuf.Reader(new Uint8Array(e.data));
      do {
        const rep = pb.Response.decodeDelimited(reader) as unknown as SlogResponse
        // console.log(reader,rep)
        const cb = this.map[rep.seq]
        if (cb) {
          if (rep.code == 200) {
            cb(null,rep.data)
          } else {
            cb(rep.msg,rep.data)
          }
        } else {
          console.log(rep)
        }
      } while (reader.pos < reader.len)
    } catch (err) {
      console.error(err)
    }
  }
}

type LogBuffOptions = {
  size: number
  invert: boolean
}


class RingLogBuff {
  public data: Message[]
  public readonly opt: LogBuffOptions
  public count = 0
  public writeIndex = 0
  constructor(opt: LogBuffOptions) {
    this.data = []
    this.opt = opt
  }
  push(m: Message) {
    if (this.count < this.opt.size) {
      this.count++
      this.data.push(m)
    } else {
      this.data[this.writeIndex] = m
      this.writeIndex++
      if (this.writeIndex == this.count) {
        this.writeIndex = 0
      }
    }
    // console.log('push',this.count,this.writeIndex)
  }
  forEach(fn: (m: Message) => void) {
    for (let i = this.writeIndex; i < this.data.length; i++) {
      fn(this.data[i])
    }
    if (this.writeIndex != 0) {
      for (let i = 0; i < this.writeIndex; i++) {
        fn(this.data[i])
      }
    }
  }
  getByInvertIndex(idx: number) {
    // const old = idx
    if (this.count < this.opt.size) {
      idx = this.count - idx - 1
    } else {
      idx = this.writeIndex - idx - 1
      if (idx < 0) {
        idx += this.count
      }
    }
    // console.log('get invert index',old,idx,this.writeIndex,this.count)
    return this.data[idx]
  }

  getByIndex(idx: number) {
    if (this.opt.invert) {
      return this.getByInvertIndex(idx)
    }
    idx += this.writeIndex
    if (idx >= this.count) {
      idx -= this.count
    }
    return this.data[idx]
  }
  clear() {
    this.data.length = 0
    this.count = 0
    this.writeIndex = 0
  }
}

type LogData = {
  refreshUI?: (n: number) => void;
  levels: RingLogBuff[]
  all: RingLogBuff
  searchResult: RingLogBuff
  currentLevel: number
  searchOpt: SearchOpt
}

const newLogData = () => {
  const opt: LogBuffOptions = {
    size: 1000,
    invert: true
  }
  const data: LogData = {
    levels: [],
    all: new RingLogBuff(opt),
    searchResult: new RingLogBuff(opt),
    currentLevel: 0,
    searchOpt: {}
  }
  for (let i = 0; i < 4; i++) {
    data.levels[i] = new RingLogBuff(opt)
  }

  return data
}

interface ItemData {
  buf: RingLogBuff
  show: (v: Message|null) => void
  filter: any
}

const useSlogClient = (ref: any) => {
  const [message,showMessage] = useState<Message|null>()
  const [realTime,setRealTime] = useState<boolean>(true)
  const [service,setServiceVal] = useState<Service|undefined>()
  const [watchFile,setWatchFile] = useState<WatchFile|null>(null)

  const logData = useRef<LogData>()
  if (!logData.current) {
    logData.current = newLogData()
  }

  const [itemData,setItemData] = useState<ItemData>({
    buf: logData.current.all,
    show: showMessage,
    filter: ref
  })

  const conn = useRef<ConnectInfo>()
  const [readyState,setReadyState] = useState<number>(0)
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [_,setRefresh] = useState<number>(0)

  logData.current.refreshUI = setRefresh

  useEffect(()=> {
    if (!service || !logData.current) {
      return
    }
    let url = (location.protocol.startsWith('https') ? 'wss': 'ws')
    if (service.path.startsWith("//")) {
      url +=  ':' + service.path + 'ws'
    } else {
      const path = '/api/slog/' + service.path + 'ws'
      url = (location.protocol.startsWith('https') ? 'wss': 'ws') + '://' + location.host + path.replace('//','/')
    }
    setReadyState(0)
    console.log('do connect ws',url)
    let ws: WebSocket
    try {
      // 'ws://192.168.123.116:8080/ws'
      // ws://fx-shaofeng.inn.sf-express.com:1080/api/slog/14/ws
      ws = new WebSocket(url)
    } catch (e) {
      console.error(e)
      return
    }
    conn.current = new ConnectInfo(ws,{
      setReadyState,
      logData: logData.current,
      refreshUI: debounce(setRefresh,500, {
        maxWait: 1000
      }),
      service
    })
    return () => {
      if (conn.current) {
        conn.current.close()
      }
      console.log('关闭ws')
    }

  },[service])

  useEffect(() => {

    if (readyState === 1 && conn.current) {
      // watch
      if (watchFile && realTime) {
        conn.current.attachFile(watchFile)
      } else {
        conn.current.attachFile(null)
      }
    }

  },[readyState,realTime,watchFile])

  const doSearch = (opt: SearchOpt) => {
    const data = logData.current
    if (!data) {
      return
    }
    console.log('搜索',opt)
    data.searchOpt = opt
    const buf = opt.level != undefined ? data.levels[opt.level]: data.all

    if (opt.keyword || opt.trace) {
      data.searchResult.clear()
      buf.forEach(m => {
        if (opt.trace) {
          if (!m.trace || !m.trace.includes(opt.trace)) {
            return
          }
        }
        if (m.message.includes(opt.keyword as string)) {
          data.searchResult.push(m)
        }
      })
      setItemData({
        buf: data.searchResult,
        show: showMessage,
        filter: ref
      })
    } else {
      setItemData({
        buf: buf,
        show: showMessage,
        filter: ref
      })
    }
  }

  return {
    logData: logData.current,
    setServiceVal,
    setWatchFile,
    message,
    showMessage,
    itemData,
    setRealTime,
    setItemData,
    service,
    watchFile,
    realTime,
    doSearch,
    readyState
  }
}

export {
  useSlogClient,
  RingLogBuff,
  LogData,
  newLogData
}
