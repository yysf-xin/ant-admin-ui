import React from 'react';
import TabLayout from "@/components/TabLayout";

const Logger: React.FC<any> = ({children, location}) => {
  return (
    <TabLayout title={false} tabList={[{
      tab: '日志分析',
      key: 'analyse',
    }, {
      tab: '日志统计',
      key: 'count'
    }]} location={location}>{children}</TabLayout>
  );
};

export default Logger;
