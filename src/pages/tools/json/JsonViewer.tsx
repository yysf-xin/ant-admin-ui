// @ts-ignore
import JSONEditor from 'jsoneditor'
import 'jsoneditor/dist/jsoneditor.css';
import {useEffect, useRef} from "react";
import {Button, Col, Row, Space} from "antd";

const topColResponsiveProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
  style: {marginTop: 12, height: 800},
};

const JsonViewer = () => {
  const container = useRef(null)
  const container2 = useRef(null)
  const editor1Ref = useRef<any>()
  const editor2Ref = useRef<any>()
  useEffect(() => {
    editor1Ref.current = new JSONEditor(container.current, {
      mode: 'code',
      onChangeText: function (jsonString: any) {
        // eslint-disable-next-line
        editor2Ref.current.updateText(jsonString)
      }
    })
    editor2Ref.current = new JSONEditor(container2.current, {
      mode: 'form',
    })
    return () => {
      editor1Ref.current.destroy()
      editor2Ref.current.destroy()
    }
  }, [])

  return <>
    <Space className={"body-background"}>
      <Button type={"primary"} onClick={() => {
        try {
          editor1Ref.current.set(editor2Ref.current.get())
        } catch (e) {
        }
      }}>右边到左边</Button>
    </Space>
    <Row gutter={8}>
      <Col ref={container} {...topColResponsiveProps}>
      </Col>
      <Col ref={container2} {...topColResponsiveProps}>
      </Col>
    </Row>
  </>
}

export default JsonViewer
