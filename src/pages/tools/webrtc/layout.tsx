import React from 'react';
import TabLayout from '@/components/TabLayout';

const Layout: React.FC<any> = ({ children, location }) => {
  return (
    <TabLayout
      title={false}
      tabList={[
        {
          tab: '屏幕捕获测试',
          key: 'screen',
        },
      ]}
      location={location}
    >
      {children}
    </TabLayout>
  );
};

export default Layout;
