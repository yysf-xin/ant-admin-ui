declare module 'simple-peer' {
  export default class Peer {
    static WEBRTC_SUPPORT: Boolean | boolean;

    constructor(props: { initiator: boolean });

    on(signal: string, param2: (data: any) => void);

    signal(data: any);

    destroy();
  }
}
