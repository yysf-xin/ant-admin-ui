import { useCallback, useEffect, useRef, useState } from 'react';
import { Alert, Button, Card, Space } from 'antd';
import Peer from 'simple-peer';

const ScreenCapture = () => {
  const videoEl = useRef<HTMLVideoElement>(null);
  const streamRef = useRef<MediaStream>();
  const [errorMessage, setErrorMessage] = useState<string>();

  const cleanFun = useCallback(() => {
    if (streamRef.current) {
      const tracks = streamRef.current.getTracks();
      tracks.forEach((track) => {
        track.stop();
      });
      if (videoEl.current) {
        videoEl.current.srcObject = null;
      }
      streamRef.current = undefined;
    }
  }, [videoEl]);

  useEffect(() => {
    return cleanFun;
  }, [cleanFun]);

  return (
    <Card
      bordered={false}
      title={
        <Space>
          <Button
            onClick={() => {
              if (
                !Peer.WEBRTC_SUPPORT ||
                !navigator.mediaDevices ||
                !navigator.mediaDevices.getDisplayMedia
              ) {
                setErrorMessage('浏览器不支持屏幕捕获');
                return;
              }
              navigator.mediaDevices
                .getDisplayMedia({
                  audio: false,
                  video: {
                    width: 1280,
                    height: 720,
                  },
                })
                .then((stream) => {
                  streamRef.current = stream;
                  if (videoEl.current) {
                    videoEl.current.srcObject = stream;
                  } else {
                    stream.stop();
                  }
                })
                .catch((reason) => {
                  setErrorMessage(reason);
                });
            }}
            type={'primary'}
          >
            捕获屏幕
          </Button>
          <Button type={'primary'} onClick={cleanFun}>
            停止
          </Button>
          <Button>测试</Button>
        </Space>
      }
    >
      <video
        ref={videoEl}
        width={600}
        height={300}
        autoPlay={true}
        controls={true}
        style={{
          maxWidth: '100%',
        }}
      />

      {errorMessage && (
        <Alert type={'error'} showIcon={true} message={errorMessage} banner={true} />
      )}
    </Card>
  );
};

export default ScreenCapture;
