import React from 'react';
import TabLayout from '@/components/TabLayout';

const Layout: React.FC<any> = ({ children, location }) => {
  return (
    <TabLayout
      title={false}
      tabList={[
        {
          tab: '代理任务',
          key: 'task',
        },
        {
          tab: '代理列表',
          key: 'canUse',
        },
      ]}
      location={location}
    >
      {children}
    </TabLayout>
  );
};

export default Layout;
