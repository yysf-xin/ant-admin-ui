import { message } from 'antd';
import ProTable, { ProColumns } from '@ant-design/pro-table';
import React from 'react';
import request from '@/services/utils/request';
import OperateColumn from '@/components/Column/OperateColumn';
import { EmptyFn } from '@/services/utils/rest';

const columns: ProColumns<any>[] = [
  {
    dataIndex: 'Id',
    key: 'Id',
    title: 'Id',
    width: 80,
    formItemProps: {
      hidden: true,
    },
    hideInSearch: true,
  },
  {
    dataIndex: 'Server',
    key: 'Server',
    title: '服务器',
  },
  {
    dataIndex: 'Port',
    key: 'Port',
    title: '端口',
  },
  {
    dataIndex: 'Type',
    key: 'Type',
    title: '类型',
  },
  {
    title: '创建时间',
    dataIndex: 'CreatedAt',
    width: 180,
    valueType: 'dateTime',
    hideInForm: true,
    hideInSearch: true,
    shouldCellUpdate: EmptyFn,
  },
  {
    title: '修改时间',
    width: 180,
    dataIndex: 'UpdatedAt',
    valueType: 'dateTime',
    hideInForm: true,
    hideInSearch: true,
    shouldCellUpdate: EmptyFn,
  },
  {
    title: '操作',
    hideInSearch: true,
    shouldCellUpdate: EmptyFn,
    render: (_, record) => {
      return OperateColumn({
        items: [
          {
            name: '使用',
            key: 'use',
          },
        ],
        onClick: (e) => {
          console.log(e, record);

          request
            .get('/server/fly/select', {
              params: {
                id: record.Id,
              },
            })
            .then((rs) => {
              if (rs.success) {
                message.success('设置成功');
              }
            });
        },
      });
    },
  },
];

const canUse: React.FC = () => {
  return (
    <ProTable
      rowKey={'Id'}
      columns={columns}
      request={() => {
        return request.get('/server/fly/canUse');
      }}
    />
  );
};

export default canUse;
