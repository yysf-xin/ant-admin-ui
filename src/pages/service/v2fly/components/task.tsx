import { Card, Input, Progress, Space, Badge, Avatar, Button, message } from 'antd';
import React, { useEffect, useRef, useState, useCallback } from 'react';
import request from '@/services/utils/request';
import { useDestroyed } from '@/services/utils/hook';

const CustomBadge: React.FC<{
  count?: number;
}> = ({ count, children }) => {
  return (
    <Badge count={count} showZero={true}>
      <Avatar shape="circle" size="large">
        {children}
      </Avatar>
    </Badge>
  );
};

const CurrView: React.FC<{
  task: TaskInfo;
}> = ({ task }) => {
  return (
    <div>
      当前选择：
      {task.Curr}
    </div>
  );
};

const Task = () => {
  const [loading, setLoading] = useState<boolean>(true);
  const [info, setTaskInfo] = useState<TaskInfo>({
    Total: 0,
    Error: 0,
  });
  const refreshTask = useRef<any>();
  const destroyed = useDestroyed();

  const Tick = useCallback(() => {
    request.get('/server/fly/status').then((rs) => {
      if (destroyed.current) {
        return;
      }
      if (rs.data == null) {
        setLoading(false);
        refreshTask.current = null;
      } else {
        if (rs.data.Total != null) {
          setTaskInfo(
            Object.assign(rs.data, {
              Percent: Math.ceil((rs.data.Checking / rs.data.Total) * 100),
            }),
          );
        } else if (rs.data.Curr){
          setTaskInfo(rs.data)
        }
        if (rs.data.Stop != null && !rs.data.Stop) {
          refreshTask.current = setTimeout(Tick, 2000);
        } else {
          setLoading(false);
        }
      }
    });
  }, [destroyed]);
  useEffect(() => {
    Tick();
    return () => {
      if (refreshTask.current) {
        clearTimeout(refreshTask.current);
      }
    };
  }, [Tick]);

  const onSearch = useCallback(
    (url: string) => {
      setLoading(true);
      request
        .post('/server/fly/check', {
          data: {
            url,
          },
        })
        .then((rs) => {
          if (destroyed.current) {
            return;
          }
          if (rs.success) {
            Tick();
          }
        });
    },
    [Tick, destroyed],
  );
//
  return (
    <>
      <Card title={'任务状态'}>
        <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
          <Input.Search
            addonBefore="代理地址"
            placeholder="请输入代理地址..."
            onSearch={onSearch}
            disabled={loading}
            enterButton
          />
          <Space>
            <Progress type="circle" percent={info.Percent} />
            <CustomBadge count={info.Total}>总共</CustomBadge>
            <CustomBadge count={info.Checking}>当前</CustomBadge>
            <CustomBadge count={info.Error}>错误</CustomBadge>
          </Space>
        </Space>
      </Card>
      <Card title={<CurrView task={info} />} className={'mt-24'}>
        <Button
          onClick={() => {
            request.get('/server/fly/save').then((rs) => {
              if (rs.success) {
                message.success('保存成功');
              }
            });
          }}
          type={'primary'}
        >
          保存当前选择
        </Button>
      </Card>
    </>
  );
};

export default Task;
