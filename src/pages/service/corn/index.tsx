// @ts-nocheck
import CronInput from "@/components/Input/CronInput";
import {createRestApi, EmptyFn} from "@/services/utils/rest";
import request from "@/services/utils/request";
import {useColumnOperate, useColumnOperates, withColumnsSwitch} from "@/services/utils/hook";
import {ProColumns} from "@ant-design/pro-table";
import CurdTable from "@/components/Rest/CurdTable";
import {PageContainer} from "@ant-design/pro-layout";
import {Button, Modal, Drawer, Input, Space} from 'antd'
import Cron from "qnn-react-cron";
import {memo, useState} from "react";
import { EditOutlined } from '@ant-design/icons';
import DictSelect from "@/components/Input/DictSelect";

const CornApi = {
  ...createRestApi('/model/corn_configs'),
  enable(id: any, Enabled: boolean) {
    return request.post(`/model/corn_configs/${id}/enable`, {
      data: {
        Enabled,
      },
    });
  },
};

const CornView = () => {

  const switchCol = withColumnsSwitch(
    (record: Proxy, checked) => {
      return CornApi.enable(record.Id, checked).then((res) => {
        if (res.success) {
          record.Enabled = checked;
        }
      });
    },
    'Enabled',
    (record) => record.Enabled,
  );
  const operateCol = useColumnOperates<CornConfig>(CornApi,()=>{
    return [
      {
        name: '运行',
        key: 'run',
      },
      {
        name: '编辑',
        key: 'edit',
      },
      {
        name: '删除',
        key: 'delete',
      },
    ]
  },(key, record)=>{
    console.log(key,record)
  });

  const columns: ProColumns<CornConfig>[] = [
    {
      dataIndex: 'Id',
      key: 'Id',
      title: 'Id',
      width: 80,
      formItemProps: {
        hidden: true,
      },
    },
    {
      title: '表达式',
      dataIndex: 'corn',
      width: 180,
      search: true,
      formItemProps: {
        required: true,
        rules: [
          {
            required: true,
          },
        ],
      },
      renderFormItem: () => {
        return <CronInput />
      },
      shouldCellUpdate: EmptyFn
    },
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
      width: 120,
      formItemProps: {
        required: true,
        rules: [
          {
            required: true,
          },
        ],
      }
    },
    {
      title: '类型',
      dataIndex: 'type',
      width: 80,
      hideInTable: true,
      formItemProps: {
        required: true,
        rules: [
          {
            required: true,
          },
        ],
      },
      renderFormItem: () =>{
        return <DictSelect dict={"corn"} />
      }
    },
    {
      title: '参数',
      dataIndex: 'param',
      width: 180,
      valueType: 'textarea',
      formItemProps: {
        required: true,
        rules: [
          {
            required: true,
          },
        ],
      },
    },
    switchCol.column,
    {
      title: '创建时间',
      dataIndex: 'CreatedAt',
      width: 180,
      valueType: 'dateTime',
      hideInForm: true,
      hideInSearch: true,
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '修改时间',
      width: 180,
      dataIndex: 'UpdatedAt',
      valueType: 'dateTime',
      hideInForm: true,
      hideInSearch: true,
      shouldCellUpdate: EmptyFn,
    },
    operateCol.column
  ]




  return <PageContainer>
    <CurdTable
      title={'定时任务管理'}
      columns={columns}
      api={CornApi}
      rowKey={'Id'}
      search={false}
      modalFormRef={operateCol.modalFormRef}
      toolBarRender={()=>[
      ]}
    />
  </PageContainer>

}

export default CornView
