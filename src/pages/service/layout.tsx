import React from 'react';
import TabLayout from '@/components/TabLayout';

const Layout: React.FC<any> = ({ children, location }) => {
  return (
    <TabLayout
      title={false}
      tabList={[
        {
          tab: '任务展示',
          key: 'task',
        },
      ]}
      location={location}
    >
      {children}
    </TabLayout>
  );
};

export default Layout;
