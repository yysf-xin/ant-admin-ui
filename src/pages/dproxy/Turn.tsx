import React from 'react';
import TabLayout from "@/components/TabLayout";

const Turn: React.FC<any> = ({children, route, location}) => {
  return (
    <TabLayout title={route.name} tabList={[{
      tab: '服务端配置',
      key: 'server',
    }, {
      tab: '已连接客户端',
      key: 'clients'
    }, {
      tab: '客户端配置',
      key: 'client',
    }]} location={location}>{children}</TabLayout>
  );
};

export default Turn;
