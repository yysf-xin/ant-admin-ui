import React from 'react';
import TabLayout from "@/components/TabLayout";

const SshServer: React.FC<any> = ({children, route, location}) => {
  return (
    <TabLayout title={route.name} tabList={[{
      tab: 'SSH列表',
      key: 'server',
    },
      {
        tab: 'SSH隧道配置',
        key: 'tunnel',
      }]} location={location}>{children}</TabLayout>
  );
};

export default SshServer;
