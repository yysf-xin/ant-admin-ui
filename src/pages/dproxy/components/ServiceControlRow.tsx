import {Col, Row} from 'antd';
import ServiceCard from '@/pages/dproxy/components/ServiceCard';

const topColResponsiveProps = {
  xs: 24,
  sm: 12,
  md: 12,
  lg: 12,
  xl: 6,
  style: {marginBottom: 24},
};

const ServiceControlRow = () => {
  return (
    <Row gutter={24}>
      <Col {...topColResponsiveProps}>
        <ServiceCard title={'代理功能'} url={'/server/proxy/enable'}/>
      </Col>
    </Row>
  );
};

export default ServiceControlRow;
