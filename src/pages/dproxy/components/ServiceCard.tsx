import {Switch} from 'antd';
import {StatisticCard} from '@ant-design/pro-card';
import request from '@/services/utils/request';
import {useCallback, useEffect, useState} from 'react';
import {useDestroyed} from '@/services/utils/hook';

const ServiceCard = ({title, url}: { title: string; url: string }) => {
  const destroyed = useDestroyed();
  const [loading, setLoading] = useState<boolean>(true);
  const [checked, setChecked] = useState<boolean>(false);
  useEffect(() => {
    request
      .get(url)
      .then((res) => {
        if (destroyed.current) {
          return;
        }
        if (res.success) {
          setChecked(res.data);
        }
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, [destroyed, url]);

  const onChange = useCallback(() => {
    setLoading(true);
    request
      .post(url, {
        data: {
          Enabled: !checked,
        },
      })
      .then(() => {
        if (!destroyed.current) {
          setLoading(false);
          setChecked(!checked);
        }
      })
      .catch(() => {
        if (!destroyed.current) {
          setLoading(false);
        }
      });
  }, [checked, destroyed, url]);

  return (
    <StatisticCard
      bordered={false}
      title={title}
      extra={<Switch loading={loading} checked={checked} onChange={onChange}/>}
      footer={'启用关闭代理服务器功能'}
    />
  );
};

export default ServiceCard;
