import CurdTable from '@/components/Rest/CurdTable';
import type { ProColumns } from '@ant-design/pro-table';
import React from 'react';
import { createRestApi } from '@/services/utils/rest';
import { useColumnOperate, withColumnsSwitch } from '@/services/utils/hook';
import request from '@/services/utils/request';

const TcpForwardApi = {
  ...createRestApi('/model/tcp_forward'),
  enable(id: any, Enabled: boolean) {
    return request.post(`/model/tcp_forward/${id}/enable`, {
      data: {
        Enabled,
      },
    });
  },
};
const TcpForwardTable: React.FC<TcpForward> = () => {
  const switchCol = withColumnsSwitch(
    (record: TcpForward, checked) => {
      return TcpForwardApi.enable(record.Id, checked).then((res) => {
        if (res.success) {
          record.Enabled = checked;
        }
      });
    },
    'Enabled',
    (record) => record.Enabled,
  );
  const operateCol = useColumnOperate(TcpForwardApi);
  const columns: ProColumns<TcpForward>[] = [
    {
      dataIndex: 'Id',
      key: 'Id',
      title: 'Id',
      width: 100,
      formItemProps: {
        hidden: true,
      },
    },
    {
      title: '名称',
      dataIndex: 'Name',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
        shouldUpdate: false,
      },
    },
    {
      title: '源地址',
      dataIndex: 'SrcIp',
      valueType: 'text',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
      },
      hideInSearch: true,
    },
    {
      title: '目的地址',
      dataIndex: 'TarIp',
      width: 180,
      valueType: 'text',
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
      },
    },
    switchCol.column,
    {
      title: '修改时间',
      width: 180,
      dataIndex: 'UpdatedAt',
      valueType: 'dateTime',
      hideInForm: true,
    },
    operateCol.column,
  ];

  return (
    <CurdTable
      title={'TCP端口转发'}
      columns={columns}
      api={TcpForwardApi}
      rowKey={'Id'}
      search={false}
      modalFormRef={operateCol.modalFormRef}
    />
  );
};
export default TcpForwardTable;
