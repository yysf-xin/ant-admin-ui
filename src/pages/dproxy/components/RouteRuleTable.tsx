import CurdTable from '@/components/Rest/CurdTable';
import type { ProColumns } from '@ant-design/pro-table';
import React from 'react';
import { createRestApi } from '@/services/utils/rest';
import { useColumnOperate, withColumnsSwitch } from '@/services/utils/hook';
import request from '@/services/utils/request';

const RouteRuleApi = {
  ...createRestApi('/model/route_rule'),
  enable(id: any, Enabled: boolean) {
    return request.post(`/model/route_rule/${id}/enable`, {
      data: {
        Enabled,
      },
    });
  },
};

const RouteRuleTable: React.FC = () => {
  const switchCol = withColumnsSwitch(
    (record: TcpForward, checked) => {
      return RouteRuleApi.enable(record.Id, checked).then((res) => {
        if (res.success) {
          record.Enabled = checked;
        }
      });
    },
    'Enabled',
    (record) => record.Enabled,
    false,
  );
  const operateCol = useColumnOperate(RouteRuleApi);
  const columns: ProColumns<TcpForward>[] = [
    {
      dataIndex: 'Id',
      key: 'Id',
      title: 'Id',
      width: 100,
      formItemProps: {
        hidden: true,
      },
    },
    {
      title: '主机地址',
      dataIndex: 'Host',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
        shouldUpdate: false,
      },
    },
    {
      title: '目的服务',
      dataIndex: 'Service',
      valueType: 'text',
      copyable: true,
      width: 240,
      formItemProps: {
        rules: [
          {
            required: false,
          },
        ],
      },
      hideInSearch: true,
    },
    switchCol.column,
    {
      title: '修改时间',
      width: 180,
      dataIndex: 'UpdatedAt',
      valueType: 'dateTime',
      hideInForm: true,
    },
    operateCol.column,
  ];

  return (
    <CurdTable
      title={'路由规则'}
      columns={columns}
      api={RouteRuleApi}
      rowKey={'Id'}
      search={false}
      modalFormRef={operateCol.modalFormRef}
    />
  );
};
export default RouteRuleTable;
