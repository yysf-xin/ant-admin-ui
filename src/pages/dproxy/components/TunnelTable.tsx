import CurdTable from '@/components/Rest/CurdTable';
import type { ProColumns } from '@ant-design/pro-table';
import React from 'react';
import { createRestApi, EmptyFn } from '@/services/utils/rest';
import { useColumnOperate, withColumnsSwitch } from '@/services/utils/hook';
import request from '@/services/utils/request';

const TunnelApi = {
  ...createRestApi('/model/tunnel'),
  enable(id: any, Enabled: boolean) {
    return request.post(`/model/tunnel/${id}/enable`, {
      data: {
        Enabled,
      },
    });
  },
};

const TunnelTable: React.FC<any> = (props) => {
  const switchCol = withColumnsSwitch(
    (record: TcpForward, checked) => {
      return TunnelApi.enable(record.Id, checked).then((res) => {
        if (res.success) {
          record.Status = checked ? 1 : 0;
        }
      });
    },
    'Status',
    (record) => record.Status === 1,
    true,
  );
  const operateCol = useColumnOperate(TunnelApi);
  const columns: ProColumns<TcpForward>[] = [
    {
      dataIndex: 'Id',
      key: 'Id',
      title: 'Id',
      width: 100,
      formItemProps: {
        hidden: true,
      },
    },
    {
      title: '名称',
      dataIndex: 'Name',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
        shouldUpdate: false,
      },
    },
    {
      title: '本地监听地址',
      dataIndex: 'Local',
      valueType: 'text',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
      },
      hideInSearch: true,
    },
    {
      title: '目标网络地址',
      dataIndex: 'Target',
      width: 180,
      valueType: 'text',
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
      },
    },
    switchCol.column,
    {
      title: '启用',
      dataIndex: 'Enabled',
      width: 80,
      valueType: 'switch',
      hideInSearch: true,
      hideInTable: true,
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '修改时间',
      width: 180,
      dataIndex: 'UpdatedAt',
      valueType: 'dateTime',
      hideInForm: true,
    },
    operateCol.column,
  ];

  return (
    <CurdTable
      title={props.route.name}
      columns={columns}
      api={TunnelApi}
      rowKey={'Id'}
      search={false}
      modalFormRef={operateCol.modalFormRef}
    />
  );
};
export default TunnelTable;
