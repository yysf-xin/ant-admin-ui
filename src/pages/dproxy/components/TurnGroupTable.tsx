import CurdTable from '@/components/Rest/CurdTable';
import type {ProColumns} from '@ant-design/pro-table';
import React from 'react';
import {createRestApi, EmptyFn} from '@/services/utils/rest';
import {useColumnOperate, withColumnsSwitch} from '@/services/utils/hook';
import request from '@/services/utils/request';

const TurnGroupApi = {
  ...createRestApi('/model/turn_group'),
  enable(id: any, Enabled: boolean) {
    return request.post(`/model/turn_group/${id}/enable`, {
      data: {
        Enabled,
      },
    });
  },
};

const TurnGroupTable: React.FC = () => {
  const switchCol = withColumnsSwitch(
    (record: TurnGroup, checked) => {
      return TurnGroupApi.enable(record.Id, checked).then((res) => {
        if (res.success) {
          record.Enabled = checked;
        }
      });
    },
    'Enabled',
    (record) => record.Enabled,
    false,
  );
  const operateCol = useColumnOperate(TurnGroupApi);
  const columns: ProColumns<TurnGroup>[] = [
    {
      dataIndex: 'Id',
      key: 'Id',
      title: 'Id',
      width: 80,
      formItemProps: {
        hidden: true,
      },
    },
    {
      title: '名称',
      dataIndex: 'Name',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
        shouldUpdate: false,
      },
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '端点地址',
      dataIndex: 'Endpoint',
      valueType: 'text',
      copyable: true,
      width: 300,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
      },
      render: (_, record) => {
        const uri = '/api/turn/' + record.Id;
        return (
          <a href={uri}>
            {location.origin}
            {uri}
          </a>
        );
      },
      hideInForm: true,
      hideInSearch: true,
      shouldCellUpdate: EmptyFn,
    },
    switchCol.column,
    {
      title: '修改时间',
      width: 180,
      dataIndex: 'UpdatedAt',
      valueType: 'dateTime',
      hideInForm: true,
      shouldCellUpdate: EmptyFn,
    },
    operateCol.column,
  ];

  return (
    <CurdTable
      title={'穿透服务'}
      columns={columns}
      api={TurnGroupApi}
      rowKey={'Id'}
      search={false}
      modalFormRef={operateCol.modalFormRef}
    />
  );
};

export default TurnGroupTable
