import {Switch} from "antd";
import {Statistic, StatisticCard} from '@ant-design/pro-card';

const ControlCard = ({title}: {
  title: string
}) => (
  <StatisticCard
    bordered={false}
    title={title}
    extra={<Switch/>}
    statistic={{
      value: 1102893
    }}
    footer={
      <Statistic value={15.1} title="累计注册数" suffix="万" layout="horizontal"/>
    }
  />
)

export default ControlCard
