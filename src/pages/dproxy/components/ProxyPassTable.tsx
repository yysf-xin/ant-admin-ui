import CurdTable from '@/components/Rest/CurdTable';
import type { ProColumns } from '@ant-design/pro-table';
import React from 'react';
import { createRestApi, EmptyFn } from '@/services/utils/rest';
import { useColumnOperate, withColumnsSwitch } from '@/services/utils/hook';
import request from '@/services/utils/request';

const ProxyPassApi = {
  ...createRestApi('/model/proxy_pass'),
  enable(id: any, Enabled: boolean) {
    return request.post(`/model/proxy_pass/${id}/enable`, {
      data: {
        Enabled,
      },
    });
  },
};

const ProxyPassTable: React.FC = () => {
  const switchCol = withColumnsSwitch(
    (record: Proxy, checked) => {
      return ProxyPassApi.enable(record.Id, checked).then((res) => {
        if (res.success) {
          record.Enabled = checked;
        }
      });
    },
    'Enabled',
    (record) => record.Enabled,
  );
  const operateCol = useColumnOperate(ProxyPassApi);
  const columns: ProColumns<Proxy>[] = [
    {
      dataIndex: 'Id',
      key: 'Id',
      title: 'Id',
      width: 80,
      formItemProps: {
        hidden: true,
      },
    },
    {
      title: '名称',
      dataIndex: 'Name',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
        shouldUpdate: false,
      },
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '域名',
      dataIndex: 'Domain',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
        shouldUpdate: false,
      },
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '路径',
      dataIndex: 'Path',
      valueType: 'text',
      copyable: true,
      width: 300,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
      },
      hideInSearch: true,
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '代理地址',
      dataIndex: 'Target',
      valueType: 'text',
      copyable: true,
      width: 300,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
      },
      hideInSearch: true,
      shouldCellUpdate: EmptyFn,
    },
    switchCol.column,
    {
      title: '修改时间',
      width: 180,
      dataIndex: 'UpdatedAt',
      valueType: 'dateTime',
      hideInForm: true,
      shouldCellUpdate: EmptyFn,
    },
    operateCol.column,
  ];

  return (
    <CurdTable
      title={'反向代理服务'}
      columns={columns}
      api={ProxyPassApi}
      rowKey={'Id'}
      search={false}
      modalFormRef={operateCol.modalFormRef}
    />
  );
};
export default ProxyPassTable;
