import CurdTable from '@/components/Rest/CurdTable';
import type { ProColumns } from '@ant-design/pro-table';
import { SshApi } from '@/services/dproxy/api';
import React from 'react';
// import { withColumnOperate } from '@/services/utils/hook';
import { EmptyFn } from '@/services/utils/rest';
import SwitchColumn from '@/components/Column/SwitchColumn';
import { useColumnOperate } from '@/services/utils/hook';

const SshServerTable: React.FC<SshServer> = () => {
  const operateCol = useColumnOperate(SshApi);
  const columns: ProColumns<SshServer>[] = [
    {
      dataIndex: 'Id',
      key: 'Id',
      title: 'Id',
      width: 80,
      formItemProps: {
        hidden: true,
      },
    },
    {
      title: '远端地址',
      dataIndex: 'Remote',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
        shouldUpdate: false,
      },
    },
    {
      title: '证书',
      dataIndex: 'IdRsa',
      valueType: 'textarea',
      copyable: true,
      width: 300,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
      },
      hideInSearch: true,
    },
    {
      title: '启用',
      dataIndex: 'Enabled',
      width: 80,
      valueType: 'switch',
      shouldCellUpdate: EmptyFn,
      render: (_, record) => {
        return (
          <SwitchColumn
            request={(r, checked) => {
              return SshApi.enable(r.Id, checked).then((res) => {
                if (res.success) {
                  r.Enabled = checked;
                }
              });
            }}
            record={record}
            checkedFn={() => record.Enabled}
          />
        );
      },
    },
    {
      title: '创建时间',
      dataIndex: 'CreatedAt',
      width: 180,
      valueType: 'dateTime',
      hideInForm: true,
    },
    {
      title: '修改时间',
      width: 180,
      dataIndex: 'UpdatedAt',
      valueType: 'dateTime',
      hideInForm: true,
    },
    operateCol.column,
  ];

  return (
    <CurdTable
      title={'SSH服务配置'}
      columns={columns}
      api={SshApi}
      rowKey={'Id'}
      search={false}
      modalFormRef={operateCol.modalFormRef}
    />
  );
};
export default SshServerTable;
