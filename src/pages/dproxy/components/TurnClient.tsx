import CurdTable from '@/components/Rest/CurdTable';
import type { ProColumns } from '@ant-design/pro-table';
import React from 'react';
import { createRestApi, EmptyFn } from '@/services/utils/rest';
import { useColumnOperate, withColumnsSwitch } from '@/services/utils/hook';
import request from '@/services/utils/request';

const TurnClientApi = {
  ...createRestApi('/model/turn_client'),
  enable(id: any, Enabled: boolean) {
    return request.post(`/model/turn_client/${id}/enable`, {
      data: {
        Enabled,
      },
    });
  },
};

const TurnClient: React.FC<TurnClient> = () => {
  const switchCol = withColumnsSwitch(
    (record: TurnClient, checked) => {
      return TurnClientApi.enable(record.Id, checked).then((res) => {
        if (res.success) {
          record.Status = checked ? 1 : 0;
        }
      });
    },
    'Status',
    (record) => record.Status === 1,
    true,
  );
  const operateCol = useColumnOperate(TurnClientApi);
  const columns: ProColumns<TurnClient>[] = [
    {
      dataIndex: 'Id',
      key: 'Id',
      title: 'Id',
      width: 80,
      formItemProps: {
        hidden: true,
      },
    },
    {
      title: '名称',
      dataIndex: 'Name',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
        shouldUpdate: false,
      },
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '端点地址',
      dataIndex: 'Endpoint',
      valueType: 'text',
      copyable: true,
      width: 300,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
      },
      hideInSearch: true,
      shouldCellUpdate: EmptyFn,
    },
    switchCol.column,
    {
      title: '启用',
      dataIndex: 'Enabled',
      width: 80,
      valueType: 'switch',
      hideInSearch: true,
      hideInTable: true,
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '修改时间',
      width: 180,
      dataIndex: 'UpdatedAt',
      valueType: 'dateTime',
      hideInForm: true,
      shouldCellUpdate: EmptyFn,
    },
    operateCol.column,
  ];

  return (
    <CurdTable
      title={'穿透服务客户端'}
      columns={columns}
      api={TurnClientApi}
      rowKey={'Id'}
      search={false}
      modalFormRef={operateCol.modalFormRef}
    />
  );
};
export default TurnClient;
