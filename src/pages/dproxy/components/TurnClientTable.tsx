import ProTable from "@ant-design/pro-table"
import {ProColumns} from "@ant-design/pro-table/lib/typing";
import request from "@/services/utils/request";

const columns: ProColumns<TurnClient>[] = [
  {
    dataIndex: 'Id',
    key: 'Id',
    title: 'Id',
    width: 80,
    formItemProps: {
      hidden: true
    }
  },
  {
    dataIndex: 'SrcIp',
    key: 'SrcIp',
    title: '客户端id',
    width: 180
  },
  {
    dataIndex: 'Time',
    key: 'Time',
    title: '持续时间',
    width: 180,
    valueType: 'dateTime'
  }
]

const TurnClientTable = () => {
  return <ProTable
    columns={columns}
    search={false}
    request={() => {
      return request.get('/turn/clients')
    }}
    bordered={true}/>
}

export default TurnClientTable
