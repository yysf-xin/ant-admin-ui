import React from 'react';
import TabLayout from "@/components/TabLayout";

const RouteRule: React.FC<any> = ({children, route, location}) => {
  return (
    <TabLayout title={route.name} tabList={[{
      tab: '代理规则',
      key: 'rule',
    },
      {
        tab: 'TCP端口转发',
        key: 'tcp',
      },
      {
        tab: '反向代理',
        key: 'proxy',
      }]} location={location}>{children}</TabLayout>
  );
};

export default RouteRule;
