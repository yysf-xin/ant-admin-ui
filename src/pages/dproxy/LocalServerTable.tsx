import CurdTable from '@/components/Rest/CurdTable';
import type { ProColumns } from '@ant-design/pro-table';
import React from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { useColumnOperate, withColumnsSwitch } from '@/services/utils/hook';
import { createRestApi, EmptyFn } from '@/services/utils/rest';
import request from '@/services/utils/request';

const LocalServerApi = {
  ...createRestApi('/model/local_server'),
  enable(id: any, enable: boolean) {
    return request.post(`/model/local_server/${id}/enable`, {
      data: {
        Status: enable ? 1 : 0,
      },
    });
  },
};

const LocalServerTable: React.FC<LocalServer> = () => {
  const switchCol = withColumnsSwitch(
    (record: LocalServer, checked) => {
      return LocalServerApi.enable(record.Id, checked).then((res) => {
        if (res.success) {
          record.Status = checked ? 1 : 0;
        }
      });
    },
    'Status',
    (record) => record.Status === 1,
    true,
  );
  const operateCol = useColumnOperate(LocalServerApi);
  const columns: ProColumns<LocalServer>[] = [
    {
      dataIndex: 'Id',
      key: 'Id',
      title: 'Id',
      width: 80,
      formItemProps: {
        hidden: true,
      },
    },
    {
      title: '域名或本地地址',
      dataIndex: 'Host',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: false,
          },
        ],
        shouldUpdate: false,
      },
    },
    {
      title: '虚拟路径',
      dataIndex: 'Path',
      valueType: 'text',
      copyable: true,
      width: 300,
      render: (e, record) => {
        let path = record.Path;
        if (record.Host) {
          path = 'http://' + record.Host + path;
        }
        return (
          <a href={path} target={'_blank'} rel="noreferrer">
            {record.Path}
          </a>
        );
      },
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
      },
      hideInSearch: true,
    },
    {
      title: '目标地址',
      dataIndex: 'Target',
      valueType: 'text',
      copyable: true,
      width: 300,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
      },
      hideInSearch: true,
    },
    switchCol.column,
    {
      title: '启用',
      dataIndex: 'Enabled',
      width: 80,
      valueType: 'switch',
      hideInSearch: true,
      hideInTable: true,
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '创建时间',
      dataIndex: 'CreatedAt',
      width: 180,
      valueType: 'dateTime',
      hideInForm: true,
    },
    {
      title: '修改时间',
      width: 180,
      dataIndex: 'UpdatedAt',
      valueType: 'dateTime',
      hideInForm: true,
    },
    operateCol.column,
  ];

  return (
    <PageContainer>
      <CurdTable
        title={'本地服务'}
        columns={columns}
        api={LocalServerApi}
        rowKey={'Id'}
        search={false}
        modalFormRef={operateCol.modalFormRef}
      />
    </PageContainer>
  );
};
export default LocalServerTable;
