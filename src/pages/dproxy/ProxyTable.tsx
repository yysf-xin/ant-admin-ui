import CurdTable from '@/components/Rest/CurdTable';
import type { ProColumns } from '@ant-design/pro-table';
import React from 'react';
import { createRestApi, EmptyFn } from '@/services/utils/rest';
import { PageContainer } from '@ant-design/pro-layout';
import { useColumnOperate, withColumnsSwitch } from '@/services/utils/hook';
import request from '@/services/utils/request';

const ProxyApi = {
  ...createRestApi('/model/proxy'),
  enable(id: any, Enabled: boolean) {
    return request.post(`/model/proxy/${id}/enable`, {
      data: {
        Enabled,
      },
    });
  },
};

const ProxyTable: React.FC<Proxy> = () => {
  const switchCol = withColumnsSwitch(
    (record: Proxy, checked) => {
      return ProxyApi.enable(record.Id, checked).then((res) => {
        if (res.success) {
          record.Enabled = checked;
        }
      });
    },
    'Enabled',
    (record) => record.Enabled,
  );
  const operateCol = useColumnOperate(ProxyApi);
  const columns: ProColumns<Proxy>[] = [
    {
      dataIndex: 'Id',
      key: 'Id',
      title: 'Id',
      width: 80,
      formItemProps: {
        hidden: true,
      },
    },
    {
      title: '代理地址',
      dataIndex: 'URL',
      copyable: true,
      width: 300,
      formItemProps: {
        rules: [
          {
            required: true,
          },
        ],
        shouldUpdate: false,
      },
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '用户名',
      dataIndex: 'UserName',
      valueType: 'text',
      copyable: true,
      width: 180,
      formItemProps: {
        rules: [
          {
            required: false,
          },
        ],
      },
      hideInSearch: true,
      shouldCellUpdate: EmptyFn,
    },
    {
      title: '密码',
      dataIndex: 'Password',
      width: 180,
      valueType: 'password',
      formItemProps: {
        rules: [
          {
            required: false,
          },
        ],
      },
      hideInSearch: true,
      hideInTable: true,
      shouldCellUpdate: EmptyFn,
    },
    switchCol.column,
    {
      title: '修改时间',
      width: 180,
      dataIndex: 'UpdatedAt',
      valueType: 'dateTime',
      hideInForm: true,
      shouldCellUpdate: EmptyFn,
    },
    operateCol.column,
  ];

  return (
    <PageContainer>
      <CurdTable
        title={'多级代理配置'}
        columns={columns}
        api={ProxyApi}
        rowKey={'Id'}
        search={false}
        modalFormRef={operateCol.modalFormRef}
      />
    </PageContainer>
  );
};
export default ProxyTable;
