import React, {Suspense} from 'react';
import {GridContent} from '@ant-design/pro-layout';
import Loading from '@/components/Loading';
import ServiceControlRow from '@/pages/dproxy/components/ServiceControlRow';

const Dashboard: React.FC = () => {
  return (
    <GridContent>
      <Suspense fallback={<Loading/>}>
        <ServiceControlRow/>
      </Suspense>
    </GridContent>
  );
};
export default Dashboard;
