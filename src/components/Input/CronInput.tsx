import { Dropdown, Input } from 'antd';
import Cron from 'qnn-react-cron';
import './CronInput.less';
const CronInput = ({ value, onChange }: { value: string; onChange: any }) => {
  return (
    <Dropdown
      trigger={['click']}
      placement="bottomCenter"
      overlayClassName={'corn-custom'}
      overlay={
        // @ts-ignore
        <Cron
          value={value}
          onOk={(v: string) => {
            onChange(v.substring(0, v.length - 2));
          }}
          panesShow={{
            year: false,
          }}
        />
      }
    >
      <Input.Search value={value} onChange={onChange} required={true} />
    </Dropdown>
  );
};

export default CronInput;
