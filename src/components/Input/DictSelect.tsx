import {Select} from "antd";
import request from "@/services/utils/request";
import {memo, useEffect, useState} from "react";
import {useDestroyed} from "@/services/utils/hook";

const dictMap = {

}

const DictSelect = ({dict,value,onChange}: {
  dict: string
  value: any,
  onChange: any
}) => {
  const [options, setOptions] = useState<[]>([]);
  const destroyed = useDestroyed()
  useEffect(()=>{
    const data = dictMap[dict]
    if (data) {
      setOptions(data)
    } else {
      request.get('/dict?key='+dict).then(rs => {
        dictMap[dict] = rs.data
        if (destroyed.current) {
          return
        }
        setOptions(rs.data)
      })
    }
  },[dict,destroyed])

  return <Select options={options} value={value} onChange={onChange} />
}

export default memo(DictSelect,(p1,p2)=>{
  return p1.value === p2.value
})
