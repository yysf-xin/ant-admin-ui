import {PageLoading} from '@ant-design/pro-layout';
import {Result} from 'antd';

const Loading = ({
                   error,
                 }: {
  error?: {
    message: string;
    name: string;
  };
}) => {
  if (error) {
    return <Result status="error" title={error.name} subTitle={error.message}/>;
  }
  return <PageLoading/>;
};
export default Loading;
