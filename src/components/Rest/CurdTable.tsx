import React, {
  useCallback,
  useEffect,
  useImperativeHandle,
  useLayoutEffect,
  useRef,
  useState,
} from 'react';
import ProTable, { ProColumns } from '@ant-design/pro-table';
import { Button, FormInstance, Modal } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import type { SearchConfig } from '@ant-design/pro-table/lib/components/Form/FormRender';
import { ActionType } from '@ant-design/pro-table/lib/typing';
import { ModalRef, RestApi } from '@/services/utils/typings';
import {ToolBarProps} from "@ant-design/pro-table/lib/components/ToolBar";

type CurdProps = {
  title: string;
  search?: false | SearchConfig;
  rowKey: string;
  columns: ProColumns[];
  api: RestApi;
  modalFormRef?: React.MutableRefObject<ModalRef | undefined>;
  toolBarRender?: ToolBarProps<any>['toolBarRender'] | false;
};

const CurdTable: React.FC<CurdProps> = (props) => {
  // console.log('CurdTable 重绘')
  const formRef = useRef<FormInstance>(null!);
  const currentModel = useRef<any>();
  const defaultModalFormRef = useRef<any>();
  const actionRef = useRef<ActionType>();
  const modalFormRef = props.modalFormRef || defaultModalFormRef;

  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [submitting, setSubmitting] = useState<boolean>(false);

  useLayoutEffect(() => {
    if (modalVisible) {
      setSubmitting(false);
      formRef.current.resetFields();
      formRef.current.setFieldsValue(currentModel.current);
    } else {
      currentModel.current = null;
    }
  }, [modalVisible]);

  useEffect(() => {
    return () => {
      // @ts-ignore
      formRef.current = undefined;
      currentModel.current = null;
    };
  }, []);

  useImperativeHandle(
    modalFormRef,
    () => ({
      editModal(modal: any) {
        currentModel.current = modal;
        setModalVisible(true);
      },
      getTableAction() {
        return actionRef.current;
      },
    }),
    [],
  );

  const shouldCellUpdate = (record: any, prev: any) => {
    return record === currentModel.current || prev !== record;
  };

  props.columns.forEach((item) => {
    if (!item.shouldCellUpdate) {
      item.shouldCellUpdate = shouldCellUpdate;
    }
  });

  const onSubmit = useCallback(
    (data) => {
      setSubmitting(true);
      let req;
      if (currentModel.current[props.rowKey]) {
        req = props.api.UpdateObj(currentModel.current[props.rowKey], data);
      } else {
        req = props.api.NewObj(data);
      }
      req.then((res: any) => {
        setSubmitting(false);
        if (res.success) {
          if (formRef.current != undefined) {
            Object.assign(currentModel.current, data);
            setModalVisible(false);
            actionRef.current?.reload();
          }
        }
      });
    },
    [props.api, props.rowKey],
  );

  return (
    <>
      <ProTable
        bordered={true}
        columns={props.columns}
        actionRef={actionRef}
        search={props.search}
        rowKey={props.rowKey}
        scroll={{ x: true }}
        pagination={{
          pageSize: 10,
        }}
        request={props.api.ListObjs}
        toolBarRender={(action,row) => {
          const bar = [<Button
            key="button"
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => {
              currentModel.current = {};
              setModalVisible(true);
            }}
          >
            新建
          </Button>]

          if (props.toolBarRender) {
            const other = props.toolBarRender(action,row)
            return [...bar,...other]
          }
          return bar
        }}
      />
      <Modal
        title={props.title}
        visible={modalVisible}
        onCancel={() => {
          setModalVisible(false);
        }}
        confirmLoading={submitting}
        onOk={() => {
          formRef.current.submit();
        }}
      >
        <ProTable
          columns={props.columns}
          bordered={true}
          type={'form'}
          form={{
            initialValues: {},
            submitter: false,
            autoComplete: 'off',
          }}
          onSubmit={onSubmit}
          formRef={formRef}
        />
      </Modal>
    </>
  );
};

export default CurdTable;
