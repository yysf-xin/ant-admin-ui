import React, {useCallback, useState} from 'react';
import {LogoutOutlined, SettingOutlined, UserOutlined} from '@ant-design/icons';
import {Avatar, Menu, Spin, Form, Modal, Input} from 'antd';
import {history, useModel} from 'umi';
import HeaderDropdown from '../HeaderDropdown';
import styles from './index.less';
import {outLogin} from '@/services/ant-design-pro/api';
import type {MenuInfo} from 'rc-menu/lib/interface';
import {redirectToLogin} from '@/services/utils/login';
import request from '@/services/utils/request';
import {useDestroyed} from '@/services/utils/hook';

export type GlobalHeaderRightProps = {
  menu?: boolean;
};

/**
 * 退出登录，并且将当前的 url 保存
 */
const loginOut = async () => {
  await outLogin();
  redirectToLogin();
};

const AvatarDropdown: React.FC<GlobalHeaderRightProps> = ({ menu }) => {
  const {initialState, setInitialState} = useModel('@@initialState');
  const [form] = Form.useForm();
  const [visible, setVisible] = useState<boolean>(false);

  const destoryed = useDestroyed();

  const onMenuClick = useCallback(
    (event: MenuInfo) => {
      const {key} = event;
      if (key === 'logout') {
        setInitialState((s) => ({...s, currentUser: undefined}));
        loginOut();
        return;
      } else if (key === 'modifyPwd') {
        setVisible(true);
        return;
      }
      history.push(`/account/${key}`);
    },
    [setInitialState],
  );

  const loading = (
    <span className={`${styles.action} ${styles.account}`}>
      <Spin
        size="small"
        style={{
          marginLeft: 8,
          marginRight: 8,
        }}
      />
    </span>
  );

  if (!initialState) {
    return loading;
  }

  const { currentUser } = initialState;

  if (!currentUser || !currentUser.name) {
    return loading;
  }

  const menuHeaderDropdown = (
    <Menu className={styles.menu} selectedKeys={[]} onClick={onMenuClick}>
      {menu && (
        <Menu.Item key="center">
          <UserOutlined />
          个人中心
        </Menu.Item>
      )}
      {menu && (
        <Menu.Item key="settings">
          <SettingOutlined/>
          个人设置
        </Menu.Item>
      )}
      {menu && <Menu.Divider/>}

      <Menu.Item key="modifyPwd">
        <LogoutOutlined/>
        修改密码
      </Menu.Item>
      <Menu.Item key="logout">
        <LogoutOutlined/>
        退出登录
      </Menu.Item>
    </Menu>
  );

  return (
    <>
      <HeaderDropdown overlay={menuHeaderDropdown}>
        <span className={`${styles.action} ${styles.account}`}>
          <Avatar size="small" className={styles.avatar} src={currentUser.avatar} alt="avatar"/>
          <span className={`${styles.name} anticon`}>{currentUser.name}</span>
        </span>
      </HeaderDropdown>
      <Modal
        visible={visible}
        title="修改密码"
        okText="修改"
        maskClosable={false}
        cancelText="取消"
        onCancel={() => {
          setVisible(false);
        }}
        onOk={() => {
          form.validateFields().then((values) => {
            request
              .post('/user/modify_password', {
                data: values,
              })
              .then((res) => {
                if (res.success && !destoryed.current) {
                  setVisible(false);
                }
              });
          });
        }}
      >
        <Form
          form={form}
          layout="vertical"
          name="form_in_modal"
          initialValues={{modifier: 'public'}}
        >
          <Form.Item
            name="old_password"
            label="旧密码"
            rules={[{required: true, message: '请输入旧密码'}]}
          >
            <Input.Password type={'password'} autoComplete={'off'}/>
          </Form.Item>

          <Form.Item
            name="password"
            label="新密码"
            rules={[
              {
                required: true,
                message: '请输入新密码',
              },
            ]}
            hasFeedback
          >
            <Input.Password autoComplete={'off'} autoSave={'off'}/>
          </Form.Item>

          <Form.Item
            name="confirm"
            label="确认密码"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: '请再次输入密码',
              },
              ({getFieldValue}) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('两次输入的密码不一致！'));
                },
              }),
            ]}
          >
            <Input.Password autoComplete={'off'}/>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default AvatarDropdown;
