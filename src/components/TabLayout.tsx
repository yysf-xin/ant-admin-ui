import {PageContainer} from '@ant-design/pro-layout';
import React, {useEffect, useState} from 'react';
import {history} from 'umi';
import {TabPaneProps} from "antd";

interface TabLayoutProp {
  children: React.ReactNode;
  location: any
  title?: React.ReactNode | false;
  tabList?: (TabPaneProps & {
    key?: React.ReactText;
  })[];
}

const TabLayout: React.FC<TabLayoutProp> = (props) => {
  const [activeKey, setActiveKey] = useState('');
  useEffect(() => {
    const key = props.location.pathname.substring(
      props.location.pathname.lastIndexOf('/') + 1,
      props.location.pathname.length,
    );
    setActiveKey(key);
  }, [props.location]);

  return (
    <PageContainer
      title={props.title}
      tabActiveKey={activeKey}
      onTabChange={(key) => {
        setActiveKey(key);
        history.replace(key);
      }}
      tabList={props.tabList}
    >
      {props.children}
    </PageContainer>
  );
};

export default TabLayout;
