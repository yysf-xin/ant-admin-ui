import {Switch} from 'antd';
import React, {useCallback, useEffect, useState} from 'react';
import {useDestroyed} from '@/services/utils/hook';

export type SwitchColumnProps = {
  record: any;
  checkedFn: (record: any) => boolean | undefined;
  request?: (record: any, checked: boolean) => Promise<any>;
};
const SwitchColumn: React.FC<SwitchColumnProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const destroyed = useDestroyed();
  useEffect(() => {
    return () => {
      setLoading(false);
    };
  }, []);
  const onClick = useCallback(() => {
    if (props.request) {
      setLoading(true);
      props.request(props.record, !props.checkedFn(props.record)).finally(() => {
        if (destroyed.current) {
          return;
        }
        setLoading(false);
      });
    }
  }, [destroyed, props]);
  return <Switch checked={props.checkedFn(props.record)} loading={loading} onClick={onClick}/>;
};

export default SwitchColumn;
