import React, {MouseEventHandler} from "react";
import {Popconfirm} from "antd";

const OperateColumn = ({items, onClick}: {
  items: {
    name: string
    key: string
    custom?: JSX.Element
  }[],
  onClick?: MouseEventHandler | undefined;
}) => {
  return (
    items.map(it => {
      if (it.custom) {
        return it.custom
      } else if (it.key === 'delete') {
        return <Popconfirm
          key={it.key}
          title="确定删除吗？"
          okButtonProps={{
            // @ts-ignore
            'data-key': it.key,
          }}
          // @ts-ignore
          onConfirm={onClick}
        >
          <a>{it.name}</a>
        </Popconfirm>
      } else {
        return <a key={it.key} onClick={onClick} data-key={it.key}>{it.name}</a>
      }
    })
  )
}
const getDataKey = (e: React.MouseEvent<Element>) => {
  const target = e.target as HTMLElement;
  return target.dataset.key ? target.dataset.key : target.parentElement?.dataset.key
}

export default OperateColumn

export {
  getDataKey
}
